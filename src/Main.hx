package ;

import com.problemmachine.anxedit.AnxEdit;
import flash.Lib;

class Main
{
	static function main() 
	{
		Lib.current.addChild(new AnxEdit(Lib.current.stage.stageWidth, Lib.current.stage.stageHeight));
	}
}