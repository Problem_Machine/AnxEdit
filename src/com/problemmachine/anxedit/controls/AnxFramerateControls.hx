package com.problemmachine.anxedit.controls;
import com.problemmachine.anxedit.event.AnxEditEvent;
import com.problemmachine.ui.LabeledButton;
import flash.display.Sprite;
import flash.errors.Error;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.text.TextFormatAlign;


class AnxFramerateControls extends Sprite
{	
	private var mButtons:Array<LabeledButton>;

	public function new(startWidth:Float, startHeight:Float) 
	{
		super();
		mButtons = [];
		for (i in 0...12)
		{
			var label:String = switch(i)
			{
				case 0: "1.0000\n1fps";
				case 1: "0.2000\n5fps";
				case 2: "0.1000\n10fps";
				case 3: "0.0833\n12fps";
				case 4: "0.0625\n16fps";
				case 5: "0.0417\n24fps";
				case 6: "0.0333\n30fps";
				case 7: "0.0208\n48fps";
				case 8: "0.0167\n60fps";
				case 9: "0.0111\n90fps";
				case 10: "0.0100\n100fps";
				case 11: "copy";
				default: throw new Error("wtf dude");
			}
			var b:LabeledButton = new LabeledButton(label, startWidth / 3, startHeight / 4, 0xFFFFFF, 0x111111);
			b.label.defaultTextFormat.align = TextFormatAlign.JUSTIFY;
			mButtons.push(b);
			b.x = (i % 3) * (startWidth / 3);
			b.y = Math.floor(i / 3) * (startHeight / 4);
			addChild(b);
			b.addEventListener(MouseEvent.CLICK, setFramerateListener);
		}
		resize(startWidth, startHeight);
		setButtonsVisible(false, false);
	}
	
	public function setButtonsVisible(visible:Bool, copyVisible:Bool):Void
	{
		for (b in mButtons)
			b.visible = visible;
		mButtons[mButtons.length - 1].visible = visible && copyVisible;
	}
	
	private function setFramerateListener(e:MouseEvent):Void
	{
		for (i in 0...mButtons.length)
		{
			var b:LabeledButton = mButtons[i];
			if (b == e.target)
			{
				var framerate:Float = switch(i)
				{
					case 0: 1.0000;
					case 1: 0.2000;
					case 2: 0.1000;
					case 3: 0.0833;
					case 4: 0.0625;
					case 5: 0.0417;
					case 6: 0.0333;
					case 7: 0.0208;
					case 8: 0.0167;
					case 9: 0.0111;
					case 10: 0.0100;
					case 11: 0;
					default: throw new Error("oh fuck off");
				}
				dispatchEvent(new AnxEditEvent(AnxEditEvent.SET_FRAMERATE, framerate));
				return;
			}
		}
	}
	
	public function resize(newWidth:Float, newHeight:Float):Void
	{
		graphics.clear();
		graphics.lineStyle(5, 0x888888);
		graphics.beginFill(0x111111);
		graphics.drawRect(2.5, 2.5, newWidth - 5, newHeight - 5);
		for (i in 0...mButtons.length)
		{
			var b:LabeledButton = mButtons[i];
			b.resize(newWidth / 3, newHeight / 4);
			b.x = (i % 3) * (newWidth / 3);
			b.y = Math.floor(i / 3) * (newHeight / 4);
		}
	}
	
}