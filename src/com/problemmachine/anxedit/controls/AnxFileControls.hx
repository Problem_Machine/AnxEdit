package com.problemmachine.anxedit.controls;
import com.problemmachine.tools.text.TextTools;
import com.problemmachine.ui.LabeledButton;
import flash.display.Graphics;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.text.TextField;
import flash.text.TextFormat;
import flash.text.TextFormatAlign;

class AnxFileControls extends Sprite
{
	public static inline var NEW_EVENT:String = "new.event";
	public static inline var SAVE_EVENT:String = "save.event";
	public static inline var SAVE_AS_EVENT:String = "as.save.event";
	public static inline var LOAD_EVENT:String = "load.event";
	public static inline var RECOMPILE_EVENT:String = "recompile.event";
	
	private var mNewButton:LabeledButton;
	private var mSaveButton:LabeledButton;
	private var mSaveAsButton:LabeledButton;
	private var mLoadButton:LabeledButton;
	private var mRecompileButton:LabeledButton;
	
	public function new(startWidth:Float, startHeight:Float) 
	{
		super();
		
		mNewButton = new LabeledButton("New", startWidth / 5, startHeight, 0xFF0000, 0x883333);
		mSaveButton = new LabeledButton("Save", startWidth / 5, startHeight, 0x00FFFF, 0x338888);
		mSaveAsButton = new LabeledButton("Save As", startWidth / 5, startHeight, 0x00FF00, 0x338833);
		mLoadButton = new LabeledButton("Load", startWidth / 5, startHeight, 0x0000FF, 0x333388);
		mRecompileButton = new LabeledButton("Recompile", startWidth / 5, startHeight, 0xFFFF00, 0x888833);
		
		mNewButton.buttonMode = mNewButton.useHandCursor = true;
		mSaveButton.buttonMode = mSaveButton.useHandCursor = true;
		mSaveAsButton.buttonMode = mSaveAsButton.useHandCursor = true;
		mLoadButton.buttonMode = mLoadButton.useHandCursor = true;
		mRecompileButton.buttonMode = mRecompileButton.useHandCursor = true;
		
		mNewButton.addEventListener(MouseEvent.CLICK, function(e:MouseEvent)
			{	dispatchEvent(new Event(NEW_EVENT));	} );
		mSaveButton.addEventListener(MouseEvent.CLICK, function(e:MouseEvent)
			{	dispatchEvent(new Event(SAVE_EVENT));	} );
		mSaveAsButton.addEventListener(MouseEvent.CLICK, function(e:MouseEvent)
			{	dispatchEvent(new Event(SAVE_AS_EVENT));	} );
		mLoadButton.addEventListener(MouseEvent.CLICK, function(e:MouseEvent)
			{	dispatchEvent(new Event(LOAD_EVENT));	} );
		mRecompileButton.addEventListener(MouseEvent.CLICK, function(e:MouseEvent)
			{	dispatchEvent(new Event(RECOMPILE_EVENT));	} );
			
		addChild(mNewButton);
		addChild(mSaveButton);
		addChild(mSaveAsButton);
		addChild(mLoadButton);
		addChild(mRecompileButton);
	
		arrange();
	}
	
	public function resize(width:Float, height:Float):Void
	{
		mNewButton.resize(width / 5, height);
		mSaveButton.resize(width / 5, height);
		mSaveAsButton.resize(width / 5, height);
		mLoadButton.resize(width / 5, height);
		mRecompileButton.resize(width / 5, height);
		
		arrange();
	}
	
	private function arrange():Void
	{
		mSaveButton.x = mNewButton.width;
		mSaveAsButton.x = mSaveButton.x + mSaveButton.width;
		mLoadButton.x = mSaveAsButton.x + mSaveAsButton.width;
		mRecompileButton.x = mLoadButton.x + mLoadButton.width;
	}
}
