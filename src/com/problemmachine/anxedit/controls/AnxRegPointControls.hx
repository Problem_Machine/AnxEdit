package com.problemmachine.anxedit.controls ;
import com.problemmachine.animation.Animation;
import com.problemmachine.animation.Frame;
import com.problemmachine.animation.event.AnimationEvent;
import com.problemmachine.anxedit.event.AnxEditEvent;
import com.problemmachine.tools.math.IntMath;
import com.problemmachine.ui.scrollwindow.ScrollWindow;
import flash.display.Sprite;
import flash.errors.Error;
import flash.events.Event;
import flash.events.FocusEvent;
import flash.events.MouseEvent;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.text.TextField;
import flash.text.TextFieldType;
import flash.text.TextFormat;


class AnxRegPointControls extends ScrollWindow
{
	private var mRegPointDisplays:Array<RegistrationPointDisplay>;
	private var mRegPoints:Array<Point>;
	private var mAddPointButton:Sprite;
	private var mDraggingPoint:Int;
	
	private var mAnimation:Animation;
	private var mSelectedFrames:Array<Int>;

	public function new(startWidth:Float, startHeight:Float) 
	{
		super(startWidth, startHeight, 0x888888, 0x111111, 15);
		
		enableHorizontalResize = false; 
		enableHorizontalScroll = false;
		enableMovement = false;
		enableVerticalResize = false;
		enableVerticalScroll = true;
		mAddPointButton = new Sprite();
		addChild(mAddPointButton);
		mAddPointButton.buttonMode = mAddPointButton.useHandCursor = true;
		mAddPointButton.addEventListener(MouseEvent.CLICK, addRegPointListener);
		mAddPointButton.visible = false;
		mRegPointDisplays = [];
		resize(startWidth, startHeight);
		setSelectedFrames();
		mDraggingPoint = -1;
	}
	
	public function setAnimation(animation:Animation):Void
	{
		if (animation != mAnimation)
		{
			if (mAnimation != null)
			{
				mAnimation.removeEventListener(AnimationEvent.ANIM_ADD_FRAME, checkFrames);
				mAnimation.removeEventListener(AnimationEvent.ANIM_REMOVE_FRAME, checkFrames);
			}
			mAnimation = animation;
			mAnimation.addEventListener(AnimationEvent.ANIM_ADD_FRAME, checkFrames);
			mAnimation.addEventListener(AnimationEvent.ANIM_REMOVE_FRAME, checkFrames);
			setSelectedFrames();
		}
		
		checkFrames();
	}
	
	private function checkFrames(?e:AnimationEvent):Void
	{
		mAddPointButton.visible = mAnimation != null && mAnimation.numFrames > 0;
	}
	
	public function setSelectedFrames(?frames:Array<Int>):Void
	{
		if (frames == null || mAnimation == null)
			frames = [];
		mSelectedFrames = frames;
		refresh();
	}
	
	@:access(com.problemmachine.animation.Frame.mRegistrationPoints)
	public function refresh():Void
	{
		while(mRegPointDisplays.length > 0)
			killRegPointDisplay(mRegPointDisplays.pop());
		if (mSelectedFrames.length > 0)
		{
			var points:Array<Point> = mAnimation.getFrame(mSelectedFrames[0]).mRegistrationPoints;
			for (p in points)
			{
				
				var s:RegistrationPointDisplay = if (mSelectedFrames.length == 1)
						new RegistrationPointDisplay(width - barThickness, 50, p);
					else
						new RegistrationPointDisplay(width - barThickness, 50, null);
				addChild(s);
				s.addEventListener(RegistrationPointDisplay.TEXT_CHANGE, regPointChangeListener);
				mRegPointDisplays.push(s);
				s.handle.addEventListener(MouseEvent.MOUSE_DOWN, startDraggingRegPoint);
				s.deleteButton.addEventListener(MouseEvent.CLICK, deleteRegPointListener);
			}
		}
		else if (mAnimation != null && mAnimation.numFrames > 0)
		{
			var len:Int = mAnimation.getFrame(0).mRegistrationPoints.length;
			for (i in 0...len)
			{
				var s:RegistrationPointDisplay = new RegistrationPointDisplay(width - barThickness, 50, null, true);
				addChild(s);
				mRegPointDisplays.push(s);
				s.handle.addEventListener(MouseEvent.MOUSE_DOWN, startDraggingRegPoint);
				s.deleteButton.addEventListener(MouseEvent.CLICK, deleteRegPointListener);
			}
		}
		arrange();
	}
	
	@:access(com.problemmachine.animation.Frame.mRegistrationPoints)
	private function regPointChangeListener(e:Event):Void
	{
		dispatchEvent(new AnxEditEvent(AnxEditEvent.SAVE_UNDO_STATE));
		var s:RegistrationPointDisplay = cast e.target;
		var num:Int = -1;
		for (i in 0...mRegPointDisplays.length)
			if (mRegPointDisplays[i] == s)
			{
				num = i;
				break;
			}
		if (num < 0)
			throw new Error("AnxRegPointControls.regPointChangeListener() - Unknown registration point display object");
		for (i in mSelectedFrames)
			mAnimation.getFrame(i).mRegistrationPoints[num] = new Point(Std.parseFloat(s.inputX.text), Std.parseFloat(s.inputY.text));
		dispatchEvent(new Event(Event.CHANGE));
	}
	
	@:access(com.problemmachine.animation.Animation.mFrames)
	@:access(com.problemmachine.animation.Frame.mRegistrationPoints)
	private function addRegPointListener(e:MouseEvent):Void
	{
		dispatchEvent(new AnxEditEvent(AnxEditEvent.SAVE_UNDO_STATE));
		for (f in mAnimation.mFrames)
			f.mRegistrationPoints.push(new Point());
		refresh();
		dispatchEvent(new Event(Event.CHANGE));
	}
	
	private function startDraggingRegPoint(e:MouseEvent):Void
	{
		var handle:Sprite = cast e.target;
		var num:Int = -1;
		for (i in 0...mRegPointDisplays.length)
			if (mRegPointDisplays[i].handle == handle)
			{
				num = i;
				break;
			}
		if (num == -1)
			throw new Error("AnxRegPointControls.startDraggingRegPoint() - Unrecognized point display");
		mDraggingPoint = num;
		var s:RegistrationPointDisplay = mRegPointDisplays[mDraggingPoint];
		removeChild(s);
		addChild(s);
		s.startDrag(false, new Rectangle(0, 0, 0, (mRegPointDisplays.length - 1) * s.height));

		panel.stage.addEventListener(MouseEvent.MOUSE_MOVE, updateDragPointDisplay);
		panel.stage.addEventListener(MouseEvent.MOUSE_UP, releaseDrag);
		updateDragPointDisplay();
	}
	
	@:access(com.problemmachine.animation.Frame.mRegistrationPoints)
	private function updateDragPointDisplay(?e:MouseEvent):Void
	{
		var len:Int = mAnimation.getFrame(0).mRegistrationPoints.length;
		var s:RegistrationPointDisplay = mRegPointDisplays[mDraggingPoint];
		
		var to:Int = IntMath.min(Math.floor(s.y / s.height), len);
		
		var newOrder:Array<Int> = [];
		for (i in 0...mRegPointDisplays.length)
			newOrder.push(i);
		if (to != mDraggingPoint)
		{
			newOrder.splice(mDraggingPoint, 1);
			newOrder.insert(to, mDraggingPoint);
		}	
		
		var h:Float = 0;
		for (i in newOrder)
		{				
			if (i != mDraggingPoint)
				mRegPointDisplays[i].y = h;
			h += s.height;
		}
	}
	
	@:access(com.problemmachine.animation.Frame.mRegistrationPoints)
	private function releaseDrag(e:MouseEvent):Void
	{
		panel.stage.removeEventListener(MouseEvent.MOUSE_MOVE, updateDragPointDisplay);
		panel.stage.removeEventListener(MouseEvent.MOUSE_UP, releaseDrag);
		mRegPointDisplays[mDraggingPoint].stopDrag();
			
		var len:Int = mAnimation.getFrame(0).mRegistrationPoints.length;
		var s:RegistrationPointDisplay = mRegPointDisplays[mDraggingPoint];
		var to:Int = IntMath.min(Math.floor(s.y / s.height), len);
			
		if (to != mDraggingPoint)
		{
			for (f in mAnimation.getAllFrames())
				f.mRegistrationPoints.insert(to, f.mRegistrationPoints.splice(mDraggingPoint, 1)[0]);
			mRegPointDisplays.insert(to, mRegPointDisplays.splice(mDraggingPoint, 1)[0]);
			dispatchEvent(new Event(Event.CHANGE));
		}
		
		mDraggingPoint = -1;
		arrange();
	}
	
	@:access(com.problemmachine.animation.Frame.mRegistrationPoints)
	private function deleteRegPointListener(e:MouseEvent):Void
	{
		dispatchEvent(new AnxEditEvent(AnxEditEvent.SAVE_UNDO_STATE));
		var s:RegistrationPointDisplay = cast e.target.parent;
		var num:Int = -1;
		for (i in 0...mRegPointDisplays.length)
			if (mRegPointDisplays[i] == s)
			{
				num = i;
				break;
			}
		mRegPointDisplays.remove(s);
		killRegPointDisplay(s);
		for (i in 0...mAnimation.numFrames)
			mAnimation.getFrame(i).mRegistrationPoints.splice(num, 1);
		dispatchEvent(new Event(Event.CHANGE));
		arrange();
	}
	private function killRegPointDisplay(display:RegistrationPointDisplay):Void
	{
		if (display.parent != null)
			removeChild(display);
		display.handle.removeEventListener(MouseEvent.MOUSE_DOWN, startDraggingRegPoint);
		display.deleteButton.removeEventListener(MouseEvent.CLICK, deleteRegPointListener);
		display.removeEventListener(RegistrationPointDisplay.TEXT_CHANGE, regPointChangeListener);
		display.dispose();
	}
	
	public function resize(newWidth:Float, newHeight:Float):Void
	{
		width = newWidth;
		height = newHeight;
		for (d in mRegPointDisplays)
			d.resize(newWidth - barThickness, 50);
		mAddPointButton.graphics.clear();
		mAddPointButton.graphics.lineStyle(3, 0x88AA88);
		mAddPointButton.graphics.beginFill(0x00FF00);
		mAddPointButton.graphics.drawRoundRect(1.5, 1.5, newWidth - barThickness - 3, 50 - 3, 3, 3);
		mAddPointButton.graphics.lineStyle();
		mAddPointButton.graphics.beginFill(0xFFFFFF);
		mAddPointButton.graphics.drawRect((newWidth - barThickness) * 0.2, 20, (newWidth - barThickness) * 0.6, 10);
		mAddPointButton.graphics.beginFill(0xFFFFFF);
		mAddPointButton.graphics.drawRect((newWidth - barThickness) * 0.4, 10, (newWidth - barThickness) * 0.2, 30);
		arrange();
	}
	
	private function arrange():Void
	{
		var h:Float = 0;
		for (d in mRegPointDisplays)
		{			
			d.y = h;
			h += d.height;
			addChild(d);
		}
		mAddPointButton.y = h;
	}
	
}

private class RegistrationPointDisplay extends Sprite
{
	public static inline var TEXT_CHANGE:String = "Event text change";
	
	public var coordinates(default, set):Point;
	public var textX:TextField;
	public var textY:TextField;
	public var inputX:TextField;
	public var inputY:TextField;
	public var deleteButton:Sprite;
	public var handle:Sprite;
	
	public function new(width:Float, height:Float, ?coordinates:Point, nullDisplay:Bool = false)
	{
		super();
		textX = new TextField();
		textY = new TextField();
		inputX = new TextField();
		inputY = new TextField();
		handle = new Sprite();
		deleteButton = new Sprite();
		if (!nullDisplay)
		{
			textX.defaultTextFormat = textY.defaultTextFormat = new TextFormat(null, 16, 0xAAAAAA);
			textX.text = "X : ";
			textY.text = "Y : ";
			textX.mouseEnabled = textY.mouseEnabled = false;
			inputX.defaultTextFormat = inputY.defaultTextFormat = new TextFormat(null, 16, 0xFFFFFF);
			inputX.background = inputY.background = inputX.border = inputY.border = true;
			inputX.backgroundColor = inputY.backgroundColor = 0x111111;
			inputX.borderColor = inputY.borderColor = 0xFFFFFF;
			inputX.addEventListener(Event.CHANGE, textListener);
			inputY.addEventListener(Event.CHANGE, textListener);
			inputX.addEventListener(FocusEvent.FOCUS_OUT, textCompleteListener);
			inputY.addEventListener(FocusEvent.FOCUS_OUT, textCompleteListener);
			inputX.type = inputY.type = TextFieldType.INPUT;
			inputX.multiline = inputY.multiline = true;
			
			addChild(textX);
			addChild(textY);
			addChild(inputX);
			addChild(inputY);
		}
		this.coordinates = coordinates;
		handle.useHandCursor = handle.buttonMode = deleteButton.useHandCursor = deleteButton.buttonMode = true;
		addChild(handle);
		addChild(deleteButton);
		resize(width, height);
	}
	
	private inline function set_coordinates(val:Point):Point
	{
		if (val != null)
		{
			coordinates = val.clone();
			inputX.text = Std.string(Math.round(val.x * 100) / 100);
			inputY.text = Std.string(Math.round(val.y * 100) / 100);
		}
		else
		{
			coordinates = null;
			inputX.text = "";
			inputY.text = "";
		}
		return val;
	}
	
	private function textListener(e:Event):Void
	{
		var s:String = "";
		var done:Bool = false;
		var dec:Bool = false;
		var tf:TextField = e.target;
		for (i in 0...tf.text.length)
		{
			var char:String = tf.text.charAt(i);
			if (char == "\n" || char == "\r")
				done = true;
			else if ((i == 0 && char == "-") ||
				(!dec && char == ".") ||
				"0123456789".indexOf(char) >= 0)
				s += char;
			if (char == ".")
				dec == true;
			
		}
		tf.text = s;
		if (done)
			stage.focus = stage;
	}
	
	private function textCompleteListener(e:FocusEvent):Void
	{
		dispatchEvent(new Event(TEXT_CHANGE));
	}
	
	public function resize(width:Float, height:Float):Void
	{
		textX.x = textY.x = width * 0.15;
		textX.width = textY.width = width * 0.3;
		inputY.y = textY.y = height * 0.5;
		inputX.width = inputY.width = width * 0.3;
		inputX.x = textX.x + textX.width;
		inputY.x = textY.x + textY.width;
		textX.height = textY.height = inputX.height = inputY.height = height * 0.45;
		graphics.clear();
		graphics.lineStyle(3, 0xAAAAAA);
		graphics.beginFill(0x333333);
		graphics.drawRect(1.5, 1.5, width - 3, height - 3);
		handle.graphics.clear();
		handle.graphics.beginFill(0x00FFFF);
		handle.graphics.drawRoundRect(1.5, 1.5, width * 0.15 - 3, height - 3, 3, 3);
		deleteButton.x = width * 0.8;
		deleteButton.graphics.clear();
		deleteButton.graphics.beginFill(0xFF0000);
		deleteButton.graphics.drawRoundRect(1.5, 1.5, width * 0.2 - 3, height - 3, 3, 3);
		deleteButton.graphics.lineStyle(1, 0x000000);
		deleteButton.graphics.moveTo(5, 5);
		deleteButton.graphics.lineTo(width * 0.2 - 5, height - 5);
		deleteButton.graphics.moveTo(width * 0.2 - 5, 5);
		deleteButton.graphics.lineTo(5, height - 5);
	}
	
	public function dispose():Void
	{
		inputX.removeEventListener(Event.CHANGE, textListener);
		inputY.removeEventListener(Event.CHANGE, textListener);
		inputX.removeEventListener(FocusEvent.FOCUS_OUT, textCompleteListener);
		inputY.removeEventListener(FocusEvent.FOCUS_OUT, textCompleteListener);
	}
}