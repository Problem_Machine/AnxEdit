package com.problemmachine.anxedit.controls ;
import com.problemmachine.animation.Animation;
import com.problemmachine.animation.AnimationMode;
import com.problemmachine.animation.Frame;
import com.problemmachine.anxedit.event.AnxEditEvent;
import com.problemmachine.ui.selector.dropdownselector.DropDownSelector;
import com.problemmachine.ui.slider.complex.LabeledSlider;
import com.problemmachine.ui.slider.Slider;
import com.problemmachine.ui.slider.SliderEvent;
import com.problemmachine.ui.slider.style.DefaultSliderStyle;
import com.problemmachine.ui.slider.style.GradientSliderStyle;
import com.problemmachine.ui.slider.style.ISliderStyle;
import com.problemmachine.ui.slider.style.RampSliderStyle;
import com.problemmachine.ui.text.NumberInput;
import flash.display.Sprite;
import flash.errors.Error;
import flash.events.Event;
import flash.events.FocusEvent;
import flash.events.MouseEvent;
import flash.text.TextField;
import flash.text.TextFormat;

class AnxFrameControls extends Sprite
{	
	private var mFrameControlsText:TextField;	
	private var mRotateSlider:LabeledSlider;
	private var mScaleSlider:LabeledSlider;
	private var mRedSlider:LabeledSlider;
	private var mGreenSlider:LabeledSlider;
	private var mBlueSlider:LabeledSlider;
	private var mAlphaSlider:LabeledSlider;
	private var mHorizontalFlipButton:Sprite;
	private var mVerticalFlipButton:Sprite;
	private var mModeSelector:DropDownSelector;
	
	private var mSelectedFrames:Array<Int>;
	private var mAnimation:Animation;
	private var mWidth:Float;
	private var mHeight:Float;

	public function new(startWidth:Float, startHeight:Float) 
	{
		super();
		mWidth = startWidth;
		mHeight = startHeight;
		
		mFrameControlsText = new TextField();
		mFrameControlsText.defaultTextFormat = new TextFormat(null, 16, 0xFFFF00);
		mFrameControlsText.mouseEnabled = false;
		mFrameControlsText.background = true;
		mFrameControlsText.backgroundColor = 0x333300;
		mFrameControlsText.border = true;
		mFrameControlsText.borderColor = 0xFFFF00;
		mFrameControlsText.width = mWidth - 1;
		mFrameControlsText.height = mHeight * 0.14;
		mFrameControlsText.text = "Frame Controls";
		
		mRotateSlider = LabeledSlider.makeDefaultLabeledSlider(0xFF7F7F7F, 0xFF999999, 0x333333, mWidth * 0.65, 
			mHeight * 0.02, mHeight * 0.12, "Rotate", mWidth * 0.25, mWidth * 0.15, 0, 1, 0.5, false, 0, 2, true,
			new TextFormat(null, 12, 0x999999));
		mScaleSlider = LabeledSlider.makeStyleLabeledSlider(new RampSliderStyle(), 0xFF7F7F7F, 0xFF999999, 0x333333, mWidth * 0.65, 
			mHeight * 0.02, mHeight * 0.12, "Scale", mWidth * 0.25, mWidth * 0.15, 0.1, 10, 1, false, 1, 2, true,
			new TextFormat(null, 12, 0x999999));
			
		mRedSlider = LabeledSlider.makeStyleLabeledSlider(new GradientSliderStyle(), 0xFFFF0000, 0xFF000000, 0x330000, mWidth * 0.65, 
			mHeight * 0.02, mHeight * 0.12, "Red", mWidth * 0.25, mWidth * 0.15, 0, 1, 0.5, false, 1, 2, true, 
			new TextFormat(null, 12, 0xFF0000));
		mGreenSlider = LabeledSlider.makeStyleLabeledSlider(new GradientSliderStyle(), 0xFF00FF00, 0xFF000000, 0x003300, mWidth * 0.65, 
			mHeight * 0.02, mHeight * 0.12, "Green", mWidth * 0.25, mWidth * 0.15, 0, 1, 0.5, false, 1, 2, true, 
			new TextFormat(null, 12, 0x00FF00));
		mBlueSlider = LabeledSlider.makeStyleLabeledSlider(new GradientSliderStyle(), 0xFF0000FF, 0xFF000000, 0x000033, mWidth * 0.65, 
			mHeight * 0.02, mHeight * 0.12, "Blue", mWidth * 0.25, mWidth * 0.15, 0, 1, 0.5, false, 1, 2, true, 
			new TextFormat(null, 12, 0x0000FF));
		mAlphaSlider = LabeledSlider.makeStyleLabeledSlider(new GradientSliderStyle(), 0xFFFFFFFF, 0xFF000000, 0x333333, mWidth * 0.65, 
			mHeight * 0.02, mHeight * 0.12, "Alpha", mWidth * 0.25, mWidth * 0.15, 0, 1, 0.5, false, 1, 2, true, 
			new TextFormat(null, 12, 0xFFFFFF));
			
		addChild(mFrameControlsText);
			
		addChild(mRotateSlider);
		addChild(mScaleSlider);
			
		addChild(mRedSlider);
		addChild(mGreenSlider);
		addChild(mBlueSlider);
		addChild(mAlphaSlider);
		
		mHorizontalFlipButton = new Sprite();
		mHorizontalFlipButton.useHandCursor = mHorizontalFlipButton.buttonMode = true;
		mHorizontalFlipButton.addEventListener(MouseEvent.CLICK, flipButtonListener);
		mVerticalFlipButton = new Sprite();
		mVerticalFlipButton.useHandCursor = mVerticalFlipButton.buttonMode = true;
		mVerticalFlipButton.addEventListener(MouseEvent.CLICK, flipButtonListener);
		
		addChild(mHorizontalFlipButton);
		addChild(mVerticalFlipButton);
		
		mRotateSlider.addEventListener(SliderEvent.ON_CHANGE, sliderListener);
		mScaleSlider.addEventListener(SliderEvent.ON_CHANGE, sliderListener);
		mRedSlider.addEventListener(SliderEvent.ON_CHANGE, sliderListener);
		mGreenSlider.addEventListener(SliderEvent.ON_CHANGE, sliderListener);
		mBlueSlider.addEventListener(SliderEvent.ON_CHANGE, sliderListener);
		mAlphaSlider.addEventListener(SliderEvent.ON_CHANGE, sliderListener);
		mRotateSlider.addEventListener(SliderEvent.ON_PRESS, saveUndoStateListener);
		mScaleSlider.addEventListener(SliderEvent.ON_PRESS, saveUndoStateListener);
		mRedSlider.addEventListener(SliderEvent.ON_PRESS, saveUndoStateListener);
		mGreenSlider.addEventListener(SliderEvent.ON_PRESS, saveUndoStateListener);
		mBlueSlider.addEventListener(SliderEvent.ON_PRESS, saveUndoStateListener);
		mAlphaSlider.addEventListener(SliderEvent.ON_PRESS, saveUndoStateListener);
		mRotateSlider.addEventListener(NumberInput.VALUE_CHANGE, saveUndoStateListener);
		mScaleSlider.addEventListener(NumberInput.VALUE_CHANGE, saveUndoStateListener);
		mRedSlider.addEventListener(NumberInput.VALUE_CHANGE, saveUndoStateListener);
		mGreenSlider.addEventListener(NumberInput.VALUE_CHANGE, saveUndoStateListener);
		mBlueSlider.addEventListener(NumberInput.VALUE_CHANGE, saveUndoStateListener);
		mAlphaSlider.addEventListener(NumberInput.VALUE_CHANGE, saveUndoStateListener);
		
		setSelectedFrames();
		resize(mWidth, mHeight);
	}
	
	public function setAnimation(?animation:Animation):Void
	{
		if (animation != mAnimation)
		{
			mAnimation = animation;
			
			setSelectedFrames();
			updateControls();
		}
	}
	
	public function setSelectedFrames(?frames:Array<Int>):Void
	{
		mSelectedFrames = frames != null ? frames.copy() : [];
		drawBack();
	}
	
	private function drawBack():Void
	{
		var active:Bool = mSelectedFrames.length > 0;
		
		mRotateSlider.visible = mScaleSlider.visible = mRedSlider.visible = 
			mGreenSlider.visible = mBlueSlider.visible = mAlphaSlider.visible = 
			mHorizontalFlipButton.visible = mVerticalFlipButton.visible = active;
		
		graphics.clear();
		graphics.lineStyle(2, active ? 0xFFFF00 : 0x666633);
		graphics.beginFill(active ? 0x666633 : 0x222211);
		graphics.drawRect(1, 1, mWidth - 2, mHeight - 2);
		mFrameControlsText.borderColor = mFrameControlsText.textColor = active ? 0xFFFF00 : 0x666633;
	}
	
	public function resize(newWidth:Float, newHeight:Float):Void
	{
		mWidth = newWidth;
		mHeight = newHeight;
		drawBack();
		
		new DefaultSliderStyle().draw(mRotateSlider.track, mRotateSlider.head, newWidth * 0.55, 
			newHeight * 0.02, newHeight * 0.04, 0xFFCCCCCC, 0xFF999999, false);
		
		new RampSliderStyle().draw(mScaleSlider.track, mScaleSlider.head, newWidth * 0.55, 
			newHeight * 0.04, newHeight * 0.08, 0xFFCCCCCC, 0xFF999999, false);
		
		var style:ISliderStyle = new GradientSliderStyle();
		style.draw(mRedSlider.track, mRedSlider.head, newWidth * 0.55, newHeight * 0.02, newHeight * 0.08, 0xFFFF0000, 0xFF000000, false);
		style.draw(mGreenSlider.track, mGreenSlider.head, newWidth * 0.55, newHeight * 0.02, newHeight * 0.08, 0xFF00FF00, 0xFF000000, false);
		style.draw(mBlueSlider.track, mBlueSlider.head, newWidth * 0.55, newHeight * 0.02, newHeight * 0.08, 0xFF0000FF, 0xFF000000, false);
		style.draw(mAlphaSlider.track, mAlphaSlider.head, newWidth * 0.55, newHeight * 0.02, newHeight * 0.08, 0xFFFFFFFF, 0xFF000000, false);
		
		mFrameControlsText.width = newWidth * 0.8 - 1;
		mFrameControlsText.height = newHeight * 0.14;
		
		drawFlipButtons(newWidth * 0.2, newHeight * 0.14);
		mHorizontalFlipButton.x = mFrameControlsText.x + mFrameControlsText.width;
		mVerticalFlipButton.x = mHorizontalFlipButton.x + mHorizontalFlipButton.width;
		
		
		mRotateSlider.label.width = newWidth * 0.25;	mRotateSlider.input.width = newWidth * 0.2 - 5;
		mScaleSlider.label.width = newWidth * 0.25;		mScaleSlider.input.width = newWidth * 0.2 - 5;
		mRedSlider.label.width = newWidth * 0.25;		mRedSlider.input.width = newWidth * 0.2 - 5;
		mGreenSlider.label.width = newWidth * 0.25;		mGreenSlider.input.width = newWidth * 0.2 - 5;
		mBlueSlider.label.width = newWidth * 0.25;		mBlueSlider.input.width = newWidth * 0.2 - 5;
		mAlphaSlider.label.width = newWidth * 0.25;		mAlphaSlider.input.width = newWidth * 0.2 - 5;
		
		mRotateSlider.x = 1;	mRotateSlider.y = newHeight * 0.14;
		mScaleSlider.x = 1;		mScaleSlider.y = newHeight * 0.28;
		
		mRedSlider.x = 1;		mRedSlider.y = newHeight * 0.42;
		mGreenSlider.x = 1;		mGreenSlider.y = newHeight * 0.56;
		mBlueSlider.x = 1;		mBlueSlider.y = newHeight * 0.7;
		mAlphaSlider.x = 1;		mAlphaSlider.y = newHeight * 0.84;
		
		mRotateSlider.refresh();	mScaleSlider.refresh();
		mRedSlider.refresh();		mGreenSlider.refresh();		
		mBlueSlider.refresh();		mAlphaSlider.refresh();
	}
	
	private function updateControls():Void
	{
		if (mSelectedFrames.length > 1)
		{
			mRotateSlider.value = mAnimation.getFrame(mSelectedFrames[0]).rotation;
			mScaleSlider.value = mAnimation.getFrame(mSelectedFrames[0]).scale;
			mRedSlider.value = mAnimation.getFrame(mSelectedFrames[0]).red;
			mGreenSlider.value = mAnimation.getFrame(mSelectedFrames[0]).green;
			mBlueSlider.value = mAnimation.getFrame(mSelectedFrames[0]).blue;
			mAlphaSlider.value = mAnimation.getFrame(mSelectedFrames[0]).alpha;
		}
		drawBack();		
		drawFlipButtons();
	}
	
	private function flipButtonListener(e:MouseEvent):Void
	{
		dispatchEvent(new AnxEditEvent(AnxEditEvent.SAVE_UNDO_STATE));
		if (e.target == mHorizontalFlipButton)
		{
			if (mSelectedFrames.length > 1)
				for (i in mSelectedFrames)
					mAnimation.getFrame(i).flipHorizontal = !mAnimation.getFrame(i).flipHorizontal;
		}
		else
		{
			if (mSelectedFrames.length > 1)
				for (i in mSelectedFrames)
					mAnimation.getFrame(i).flipVertical = !mAnimation.getFrame(i).flipVertical;
		}
		drawFlipButtons();
	}
	
	private function drawFlipButtons(?bothWidth:Float, ?height:Float):Void
	{
		var w:Float = bothWidth != null ? bothWidth * 0.5 : mHorizontalFlipButton.width;
		var h:Float = height != null ? height : mHorizontalFlipButton.height;
		mHorizontalFlipButton.graphics.clear();
		mVerticalFlipButton.graphics.clear();
		if (mAnimation == null || mSelectedFrames.length <= 0)
			return;
		var horizontal:Bool = true;
		var vertical:Bool = true;
		for (i in mSelectedFrames)
		{
			if (!mAnimation.getFrame(i).flipHorizontal)
				horizontal = false;
			if (!mAnimation.getFrame(i).flipVertical)
				vertical = false;
		}
		if (horizontal)
		{
			mHorizontalFlipButton.graphics.lineStyle(2, 0xFFFF00, 1);
			mHorizontalFlipButton.graphics.beginFill(0x666633);
		}
		else
		{
			mHorizontalFlipButton.graphics.lineStyle(2, 0x666633, 1);
			mHorizontalFlipButton.graphics.beginFill(0x333300);
		}
		mHorizontalFlipButton.graphics.drawRect(1, 1, w - 2, h - 2);
		if (mAnimation!= null && mAnimation.flipHorizontal)
			mHorizontalFlipButton.graphics.lineStyle(2, 0xFFFFFF, 1);
			
		mHorizontalFlipButton.graphics.moveTo(w * 0.2, h * 0.5);
		mHorizontalFlipButton.graphics.lineTo(w * 0.3, h * 0.6);
		mHorizontalFlipButton.graphics.lineTo(w * 0.3, h * 0.4);
		mHorizontalFlipButton.graphics.lineTo(w * 0.2, h * 0.5);
		mHorizontalFlipButton.graphics.lineTo(w * 0.8, h * 0.5);
		mHorizontalFlipButton.graphics.lineTo(w * 0.7, h * 0.6);
		mHorizontalFlipButton.graphics.lineTo(w * 0.7, h * 0.4);
		mHorizontalFlipButton.graphics.lineTo(w * 0.8, h * 0.5);
		if (vertical)
		{
			mVerticalFlipButton.graphics.lineStyle(2, 0xFFFF00, 1);
			mVerticalFlipButton.graphics.beginFill(0x666633);
		}
		else
		{
			mVerticalFlipButton.graphics.lineStyle(2, 0x666633, 1);
			mVerticalFlipButton.graphics.beginFill(0x333300);
		}
		mVerticalFlipButton.graphics.drawRect(1, 1, w - 2, h - 2);
		if (mAnimation!= null && mAnimation.flipVertical)
			mVerticalFlipButton.graphics.lineStyle(2, 0xFFFFFF, 1);
			
		mVerticalFlipButton.graphics.moveTo(w * 0.5, h * 0.2);
		mVerticalFlipButton.graphics.lineTo(w * 0.6, h * 0.3);
		mVerticalFlipButton.graphics.lineTo(w * 0.4, h * 0.3);
		mVerticalFlipButton.graphics.lineTo(w * 0.5, h * 0.2);
		mVerticalFlipButton.graphics.lineTo(w * 0.5, h * 0.8);
		mVerticalFlipButton.graphics.lineTo(w * 0.6, h * 0.7);
		mVerticalFlipButton.graphics.lineTo(w * 0.4, h * 0.7);
		mVerticalFlipButton.graphics.lineTo(w * 0.5, h * 0.8);
	}
	
	private function sliderListener(e:SliderEvent):Void
	{
		if (mAnimation == null)
			return;
		if (e.source == mRotateSlider.slider)
		{
			for (i in mSelectedFrames)
			{
				var f:Frame = mAnimation.getFrame(i);
				f.rotation = mRotateSlider.value;
				mRotateSlider.value = f.rotation;
			}
		}
		else if (e.source == mScaleSlider.slider)
		{
			for (i in mSelectedFrames)
			{
				var f:Frame = mAnimation.getFrame(i);
				f.scale = mScaleSlider.value;
				mScaleSlider.value = f.scale;
			}
		}
		else if (e.source == mRedSlider.slider)
		{
			for (i in mSelectedFrames)
			{
				var f:Frame = mAnimation.getFrame(i);
				f.red = mRedSlider.value;
				mRedSlider.value = f.red;
			}
		}
		else if (e.source == mGreenSlider.slider)
		{
			for (i in mSelectedFrames)
			{
				var f:Frame = mAnimation.getFrame(i);
				f.green = mGreenSlider.value;
				mGreenSlider.value = f.green;
			}
		}
		else if (e.source == mBlueSlider.slider)
		{
			for (i in mSelectedFrames)
			{
				var f:Frame = mAnimation.getFrame(i);
				f.blue = mBlueSlider.value;
				mBlueSlider.value = f.blue;
			}
		}
		else if (e.source == mAlphaSlider.slider)
		{
			for (i in mSelectedFrames)
			{
				var f:Frame = mAnimation.getFrame(i);
				f.alpha = mAlphaSlider.value;
				mAlphaSlider.value = f.alpha;
			}
		}
		else
			throw new Error("bad slider");
	}
	
	private inline function saveUndoStateListener(e:Event):Void
	{
		dispatchEvent(new AnxEditEvent(AnxEditEvent.SAVE_UNDO_STATE));
	}
}