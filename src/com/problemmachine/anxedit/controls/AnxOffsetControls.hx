package com.problemmachine.anxedit.controls ;
import com.problemmachine.animation.Animation;
import com.problemmachine.animation.Frame;
import com.problemmachine.anxedit.event.AnxEditEvent;
import flash.display.Graphics;
import flash.display.Sprite;
import flash.errors.Error;
import flash.events.Event;
import flash.events.FocusEvent;
import flash.events.MouseEvent;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.text.TextField;
import flash.text.TextFieldType;
import flash.text.TextFormat;


class AnxOffsetControls extends Sprite
{
	private var mWidth:Float;
	private var mHeight:Float;
	
	private var mUpperLeftButton:Sprite;
	private var mUpperMiddleButton:Sprite;
	private var mUpperRightButton:Sprite;
	private var mMidLeftButton:Sprite;
	private var mCenterButton:Sprite;
	private var mMidRightButton:Sprite;
	private var mLowerLeftButton:Sprite;
	private var mLowerMiddleButton:Sprite;
	private var mLowerRightButton:Sprite;
	private var mCoordinateDisplay:Sprite;
	private var mCoordinateTypeDisplay:TextField;
	private var mCoordinateDisplayX:TextField;
	private var mCoordinateInputX:TextField;
	private var mCoordinateDisplayY:TextField;
	private var mCoordinateInputY:TextField;
	
	private var mAnimation:Animation;
	private var mSelectedFrames:Array<Int>;

	public function new(startWidth:Float, startHeight:Float) 
	{
		super();
		
		mUpperLeftButton = new Sprite();
		mUpperMiddleButton = new Sprite();
		mUpperRightButton = new Sprite();
		mMidLeftButton = new Sprite();
		mCenterButton = new Sprite();
		mMidRightButton = new Sprite();
		mLowerLeftButton = new Sprite();
		mLowerMiddleButton = new Sprite();
		mLowerRightButton = new Sprite();
		
		mUpperLeftButton.addEventListener(MouseEvent.CLICK, buttonListener);
		mUpperMiddleButton.addEventListener(MouseEvent.CLICK, buttonListener);
		mUpperRightButton.addEventListener(MouseEvent.CLICK, buttonListener);
		mMidLeftButton.addEventListener(MouseEvent.CLICK, buttonListener);
		mCenterButton.addEventListener(MouseEvent.CLICK, buttonListener);
		mMidRightButton.addEventListener(MouseEvent.CLICK, buttonListener);
		mLowerLeftButton.addEventListener(MouseEvent.CLICK, buttonListener);
		mLowerMiddleButton.addEventListener(MouseEvent.CLICK, buttonListener);
		mLowerRightButton.addEventListener(MouseEvent.CLICK, buttonListener);		
		
		mCoordinateDisplay = new Sprite();
		mCoordinateTypeDisplay = new TextField();
		mCoordinateDisplayX = new TextField();
		mCoordinateInputX = new TextField();
		mCoordinateDisplayY = new TextField();
		mCoordinateInputY = new TextField();
		mCoordinateTypeDisplay.defaultTextFormat = new TextFormat(null, 12, 0xAAAAAA);
		mCoordinateDisplayX.defaultTextFormat = mCoordinateDisplayY.defaultTextFormat = new TextFormat(null, 16, 0xAAAAAA);
		mCoordinateDisplayX.text = "X : ";
		mCoordinateDisplayY.text = "Y : ";
		mCoordinateInputX.defaultTextFormat = mCoordinateInputY.defaultTextFormat = new TextFormat(null, 16, 0xFFFFFF);
		mCoordinateInputX.border = mCoordinateInputX.background = mCoordinateInputY.border = mCoordinateInputY.background = true;
		mCoordinateInputX.backgroundColor = mCoordinateInputY.backgroundColor = 0x111111;
		mCoordinateInputX.type = mCoordinateInputY.type = TextFieldType.INPUT;
		mCoordinateInputX.addEventListener(Event.CHANGE, textListener);
		mCoordinateInputX.addEventListener(FocusEvent.FOCUS_OUT, textCompleteListener);
		mCoordinateInputY.addEventListener(Event.CHANGE, textListener);
		mCoordinateInputY.addEventListener(FocusEvent.FOCUS_OUT, textCompleteListener);
		
		addChild(mUpperLeftButton);
		addChild(mUpperMiddleButton);
		addChild(mUpperRightButton);
		addChild(mMidLeftButton);
		addChild(mCenterButton);
		addChild(mMidRightButton);
		addChild(mLowerLeftButton);
		addChild(mLowerMiddleButton);
		addChild(mLowerRightButton);
		
		mUpperLeftButton.useHandCursor = mUpperLeftButton.buttonMode = true;
		mUpperMiddleButton.useHandCursor = mUpperMiddleButton.buttonMode = true;
		mUpperRightButton.useHandCursor = mUpperRightButton.buttonMode = true;
		mMidLeftButton.useHandCursor = mMidLeftButton.buttonMode = true;
		mCenterButton.useHandCursor = mCenterButton.buttonMode = true;
		mMidRightButton.useHandCursor = mMidRightButton.buttonMode = true;
		mLowerMiddleButton.useHandCursor = mLowerMiddleButton.buttonMode = true;
		mLowerMiddleButton.useHandCursor = mLowerMiddleButton.buttonMode = true;
		mLowerRightButton.useHandCursor = mLowerRightButton.buttonMode = true;
		
		addChild(mCoordinateDisplay);
		mCoordinateDisplay.addChild(mCoordinateTypeDisplay);
		mCoordinateDisplay.addChild(mCoordinateDisplayX);
		mCoordinateDisplay.addChild(mCoordinateInputX);
		mCoordinateDisplay.addChild(mCoordinateDisplayY);
		mCoordinateDisplay.addChild(mCoordinateInputY);
		
		mSelectedFrames = [];
		resize(startWidth, startHeight);
	}
	
	public function setAnimation(animation:Animation):Void
	{
		if (animation != mAnimation)
		{
			mAnimation = animation;
			setSelectedFrames();
		}
	}
	
	public function setSelectedFrames(?frames:Array<Int>):Void
	{
		if (frames == null)
			frames = [];
		
		mSelectedFrames = frames.copy();
		draw();
	}
	
	public function resize(newWidth:Float, newHeight:Float):Void
	{
		mWidth = newWidth;
		mHeight = newHeight;
		
		mUpperLeftButton.x = 0; mUpperLeftButton.y = 0;
		mUpperMiddleButton.x = mWidth * 0.2; mUpperMiddleButton.y = 0;
		mUpperRightButton.x = mWidth * 0.4; mUpperRightButton.y = 0;
		mMidLeftButton.x = 0; mMidLeftButton.y = mHeight / 3;
		mCenterButton.x = mWidth * 0.2; mCenterButton.y = mHeight / 3;
		mMidRightButton.x = mWidth * 0.4; mMidRightButton.y = mHeight/ 3;
		mLowerLeftButton.x = 0; mLowerLeftButton.y = mHeight * 2 / 3;
		mLowerMiddleButton.x = mWidth * 0.2; mLowerMiddleButton.y = mHeight * 2 / 3;
		mLowerRightButton.x = mWidth * 0.4; mLowerRightButton.y = mHeight * 2 / 3;
		mCoordinateDisplay.x = mWidth * 0.6; mCoordinateDisplay.y = 0;
		mCoordinateTypeDisplay.width = mWidth * 0.4;
		mCoordinateTypeDisplay.x = 0;	mCoordinateTypeDisplay.y = 10;
		mCoordinateDisplayX.width = mCoordinateDisplayY.width = mWidth * 0.18;
		mCoordinateInputX.width = mCoordinateInputY.width = mWidth * 0.18;
		mCoordinateDisplayX.x = 0;	
		mCoordinateInputX.x = mCoordinateDisplayX.x + mCoordinateDisplayX.width;	
		mCoordinateDisplayY.x = 0;	
		mCoordinateInputY.x = mCoordinateDisplayY.x + mCoordinateDisplayY.width;	
		mCoordinateDisplayX.y = mCoordinateInputX.y = mHeight / 3;
		mCoordinateDisplayY.y = mCoordinateInputY.y = mHeight * 2 / 3;
		mCoordinateTypeDisplay.height = mCoordinateDisplayX.height = mCoordinateInputX.height = 
			mCoordinateDisplayY.height = mCoordinateInputY.height = mHeight * 0.2;
		
		draw();
	}
	
	private function buttonListener(e:MouseEvent):Void
	{
		dispatchEvent(new AnxEditEvent(AnxEditEvent.SAVE_UNDO_STATE));
		var frames:Array<Int> = mSelectedFrames.copy();
		if (frames.length <= 0)
			for (i in 0...mAnimation.numFrames)
				frames.push(i);
		for (i in frames)
		{
			var f:Frame = mAnimation.getFrame(i);
			f.offset = 
				if (e.target == mUpperLeftButton)
					new Point(0, 0);
				else if (e.target == mUpperMiddleButton)
					new Point(-f.clipRect.width * 0.5, 0);
				else if (e.target == mUpperRightButton)
					new Point(-f.clipRect.width, 0);
				else if (e.target == mMidLeftButton)
					new Point(0, -f.clipRect.height * 0.5);
				else if (e.target == mCenterButton)
					new Point(-f.clipRect.width * 0.5, -f.clipRect.height * 0.5);
				else if (e.target == mMidRightButton)
					new Point(-f.clipRect.width, -f.clipRect.height * 0.5);
				else if (e.target == mLowerLeftButton)
					new Point(0, -f.clipRect.height);
				else if (e.target == mLowerMiddleButton)
					new Point(-f.clipRect.width * 0.5, -f.clipRect.height);
				else if (e.target == mLowerRightButton)
					new Point(-f.clipRect.width, -f.clipRect.height);
				else
					throw new Error("AnxOffsetControls.buttonListener() : Unrecognized button");
		}
		draw();
	}
	
	private function textListener(e:Event):Void
	{
		var s:String = "";
		var done:Bool = false;
		var dec:Bool = false;
		var tf:TextField = e.target;
		for (i in 0...tf.text.length)
		{
			var char:String = tf.text.charAt(i);
			if ((i == 0 && char == "-") ||
				(!dec && char == ".") ||
				"0123456789".indexOf(char) >= 0)
				s += char;
			if (char == ".")
				dec == true;
			if (char == "\n" || char == "\r")
			{
				done = true;
				break;
			}
		}
		tf.text = s;
		if (done)
			stage.focus = stage;
	}
	
	private function textCompleteListener(e:FocusEvent):Void
	{
		dispatchEvent(new AnxEditEvent(AnxEditEvent.SAVE_UNDO_STATE));
		var tf:TextField = e.target;
		var frames:Array<Int> = mSelectedFrames.copy();
		if (frames.length <= 0)
			for (i in 0...mAnimation.numFrames)
				frames.push(i);
		if (tf == mCoordinateInputX)
		{
			for (i in frames)
				mAnimation.getFrame(i).offset.x = Std.parseFloat(tf.text);
		}
		else
		{
			for (i in frames)
				mAnimation.getFrame(i).offset.y = Std.parseFloat(tf.text);
		}
		draw();
	}
	
	private function updateText():Void
	{
		if (mAnimation == null)
		{
			mCoordinateInputX.text = mCoordinateInputY.text = "";
			return;
		}
		var frames:Array<Int> = mSelectedFrames.copy();
		if (frames.length <= 0)
			for (i in 0...mAnimation.numFrames)
				frames.push(i);
		if (frames.length <= 0)
			return;
		var x:Float = mAnimation.getFrame(frames[0]).offset.x;
		var y:Float = mAnimation.getFrame(frames[0]).offset.y;
		for (i in 1...frames.length)
		{
			var f:Frame = mAnimation.getFrame(frames[i]);
			if (f.offset.x != x)
				x = Math.NaN;
			if (f.offset.y != y)
				y = Math.NaN;
		}
		mCoordinateInputX.text = !Math.isNaN(x) ? Std.string(Math.round(x * 100) / 100) : "";
		mCoordinateInputY.text = !Math.isNaN(y) ? Std.string(Math.round(y * 100) / 100) : "";
	}
	
	private function draw():Void
	{
		var activeButton:Sprite = getCurrentlyActiveButton();
		graphics.clear();
		if (mAnimation != null)
		{
			graphics.lineStyle(2, 0xFFFFFF);
			graphics.beginFill(0x444444);
		}
		else
		{
			graphics.lineStyle(2, 0x888888);
			graphics.beginFill(0x111111);
		}
		
		graphics.drawRect(1, 1, mWidth - 2, mHeight - 2);
		drawButtons(activeButton);
			
		mCoordinateTypeDisplay.text = 
			if (activeButton == mUpperLeftButton)
				"Upper-Left";
			else if (activeButton == mUpperMiddleButton)
				"Upper-Center";
			else if (activeButton == mUpperRightButton)
				"Upper-Right";
			else if (activeButton == mMidLeftButton)
				"Center-Left";
			else if (activeButton == mCenterButton)
				"Center";
			else if (activeButton == mMidRightButton)
				"Center-Right";
			else if (activeButton == mLowerLeftButton)
				"Lower-Left";
			else if (activeButton == mLowerMiddleButton)
				"Lower-Center";
			else if (activeButton == mLowerRightButton)
				"Lower-Right";
			else
				"Custom/Mixed";
		updateText();
	}
	
	public function getCurrentOffset():Point
	{
		
		var frames:Array<Int> = mSelectedFrames.copy();
		if (frames.length <= 0)
			for (i in 0...mAnimation.numFrames)
				frames.push(i);
		if (frames.length <= 0)
			return null;
		var p:Point = mAnimation.getFrame(frames[0]).offset;
		for (i in 1...frames.length)
			if (!mAnimation.getFrame(frames[i]).offset.equals(p))
				return null;
		return p;
	}
	
	private function drawButtons(activeButton:Sprite):Void
	{
		mUpperLeftButton.visible = mUpperMiddleButton.visible = mUpperRightButton.visible = 
			mMidLeftButton.visible = mCenterButton.visible = mMidRightButton.visible = 
			mLowerLeftButton.visible = mLowerMiddleButton.visible = mLowerRightButton.visible = 
			mCoordinateDisplay.visible = true;
		
		var color1:UInt = 0x888888;
		var color2:UInt = 0x111111;
		var selectColor:UInt = 0xFFFFFF;
		drawBaseButton(mUpperLeftButton, mWidth * 0.2, mHeight / 3, mUpperLeftButton == activeButton ? selectColor : color1, color2);
		drawBaseButton(mUpperMiddleButton, mWidth * 0.2, mHeight / 3, mUpperMiddleButton == activeButton ? selectColor : color1, color2);
		drawBaseButton(mUpperRightButton, mWidth * 0.2, mHeight / 3, mUpperRightButton == activeButton ? selectColor : color1, color2);
		drawBaseButton(mMidLeftButton, mWidth * 0.2, mHeight / 3, mMidLeftButton == activeButton ? selectColor : color1, color2);
		drawBaseButton(mCenterButton, mWidth * 0.2, mHeight / 3, mCenterButton == activeButton ? selectColor : color1, color2);
		drawBaseButton(mMidRightButton, mWidth * 0.2, mHeight / 3, mMidRightButton == activeButton ? selectColor : color1, color2);
		drawBaseButton(mLowerLeftButton, mWidth * 0.2, mHeight / 3, mLowerLeftButton == activeButton ? selectColor : color1, color2);
		drawBaseButton(mLowerMiddleButton, mWidth * 0.2, mHeight / 3, mLowerMiddleButton == activeButton ? selectColor : color1, color2);
		drawBaseButton(mLowerRightButton, mWidth * 0.2, mHeight / 3, mLowerRightButton == activeButton ? selectColor : color1, color2);
		
		mUpperLeftButton.graphics.beginFill(0x333333);
		mUpperLeftButton.graphics.lineStyle(3, mUpperLeftButton == activeButton ? selectColor : color1);
		mUpperLeftButton.graphics.moveTo(1.5, mHeight / 6);		mUpperLeftButton.graphics.lineTo(mWidth * 0.1, 1.5);
		mUpperMiddleButton.graphics.beginFill(0x333333);
		mUpperMiddleButton.graphics.lineStyle(3, mUpperMiddleButton == activeButton ? selectColor : color1);
		mUpperMiddleButton.graphics.moveTo(1.5, mHeight / 6);		
		mUpperMiddleButton.graphics.lineTo(mWidth * 0.1, 1.5);	mUpperMiddleButton.graphics.lineTo(mWidth * 0.2 - 1.5, mHeight / 6);
		mUpperRightButton.graphics.beginFill(0x333333);
		mUpperRightButton.graphics.lineStyle(3, mUpperRightButton == activeButton ? selectColor : color1);
		mUpperRightButton.graphics.moveTo(mWidth * 0.2 - 1.5, mHeight / 6);		mUpperRightButton.graphics.lineTo(mWidth * 0.1, 1.5);
		mMidLeftButton.graphics.beginFill(0x333333);
		mMidLeftButton.graphics.lineStyle(3, mMidLeftButton == activeButton ? selectColor : color1);
		mMidLeftButton.graphics.moveTo(mWidth * 0.1, 1.5);		
		mMidLeftButton.graphics.lineTo(1.5, mHeight / 6);			mMidLeftButton.graphics.lineTo(mWidth * 0.1, mHeight / 3 - 1.5);
		mCenterButton.graphics.beginFill(0x333333);
		mCenterButton.graphics.lineStyle(3, mCenterButton == activeButton ? selectColor : color1);
		mCenterButton.graphics.drawCircle(mWidth * 0.1, mHeight / 6, 10);
		mMidRightButton.graphics.beginFill(0x333333);
		mMidRightButton.graphics.lineStyle(3, mMidRightButton == activeButton ? selectColor : color1);
		mMidRightButton.graphics.moveTo(mWidth * 0.1, 1.5);		
		mMidRightButton.graphics.lineTo(mWidth * 0.2 - 1.5, mHeight / 6);	mMidRightButton.graphics.lineTo(mWidth * 0.1, mHeight / 3 - 1.5);
		mLowerLeftButton.graphics.beginFill(0x333333);
		mLowerLeftButton.graphics.lineStyle(3, mLowerLeftButton == activeButton ? selectColor : color1);
		mLowerLeftButton.graphics.moveTo(1.5, mHeight / 6);		mLowerLeftButton.graphics.lineTo(mWidth * 0.1, mHeight / 3 - 1.5);
		mLowerMiddleButton.graphics.beginFill(0x333333);
		mLowerMiddleButton.graphics.lineStyle(3, mLowerMiddleButton == activeButton ? selectColor : color1);
		mLowerMiddleButton.graphics.moveTo(1.5, mHeight / 6);		
		mLowerMiddleButton.graphics.lineTo(mWidth * 0.1, mHeight / 3 - 1.5);	mLowerMiddleButton.graphics.lineTo(mWidth * 0.2 - 1.5, mHeight / 6);
		mLowerRightButton.graphics.beginFill(0x333333);
		mLowerRightButton.graphics.lineStyle(3, mLowerRightButton == activeButton ? selectColor : color1);
		mLowerRightButton.graphics.moveTo(mWidth * 0.1, mHeight / 3 - 1.5);		mLowerRightButton.graphics.lineTo(mWidth * 0.2 - 1.5, mHeight / 6);
	}
	
	private function getCurrentlyActiveButton():Sprite
	{
		if (mAnimation == null)
			return null;
			
		var activeButton:Sprite = null;
		var customPoint:Point = null;
		var frames:Array<Int> = mSelectedFrames.copy();
		if (frames.length <= 0)
			for (i in 0...mAnimation.numFrames)
				frames.push(i);
		if (frames.length <= 0)
			return null;
		for (i in frames)
		{
			var f:Frame = mAnimation.getFrame(i);
			var r:Rectangle = f.clipRect;
			if (r == null)
				return null;
			var offset:Point = f.offset;
			var s:Sprite = null;
			if (offset.y == 0)
			{
				if (offset.x == 0)
					s = mUpperLeftButton;
				else if (offset.x == -f.clipRect.width * 0.5)
					s = mUpperMiddleButton;
				else if (offset.x == -f.clipRect.width)
					s = mUpperRightButton;
			}
			else if (offset.y == -f.clipRect.height * 0.5)
			{
				if (offset.x == 0)
					s = mMidLeftButton;
				else if (offset.x == -f.clipRect.width * 0.5)
					s = mCenterButton;
				else if (offset.x == -f.clipRect.width)
					s = mMidRightButton;
			}
			else if (offset.y == -f.clipRect.height)
			{
				if (offset.x == 0)
					s = mLowerLeftButton;
				else if (offset.x == -f.clipRect.width * 0.5)
					s = mLowerMiddleButton;
				else if (offset.x == -f.clipRect.width)
					s = mLowerRightButton;
			}
			if (activeButton != null && s != activeButton)
				return null;
			activeButton = s;
		}
		return activeButton;
	}
	
	private function drawBaseButton(sprite:Sprite, width:Float, height:Float, color1:UInt, color2:UInt):Void
	{
		var g:Graphics = sprite.graphics;
		g.clear();
		g.lineStyle(2, color1);
		g.beginFill(color2);
		g.drawRect(1, 1, width - 2, height - 2);
	}
}