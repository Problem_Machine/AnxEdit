package com.problemmachine.anxedit.controls ;
import com.problemmachine.animation.Animation;
import com.problemmachine.animation.AnimationMode;
import com.problemmachine.anxedit.event.AnxEditEvent;
import com.problemmachine.ui.selector.dropdownselector.DropDownSelector;
import com.problemmachine.ui.selector.event.SelectorEvent;
import com.problemmachine.ui.slider.complex.LabeledSlider;
import com.problemmachine.ui.slider.Slider;
import com.problemmachine.ui.slider.SliderEvent;
import com.problemmachine.ui.slider.style.DefaultSliderStyle;
import com.problemmachine.ui.slider.style.GradientSliderStyle;
import com.problemmachine.ui.slider.style.ISliderStyle;
import com.problemmachine.ui.slider.style.RampSliderStyle;
import com.problemmachine.ui.text.NumberInput;
import flash.display.Sprite;
import flash.errors.Error;
import flash.events.Event;
import flash.events.FocusEvent;
import flash.events.MouseEvent;
import flash.text.TextField;
import flash.text.TextFormat;

class AnxAnimationControls extends Sprite
{	
	private var mAnimationControlsText:TextField;	
	private var mRotateSlider:LabeledSlider;
	private var mScaleSlider:LabeledSlider;
	private var mRedSlider:LabeledSlider;
	private var mGreenSlider:LabeledSlider;
	private var mBlueSlider:LabeledSlider;
	private var mAlphaSlider:LabeledSlider;
	private var mSpeedSlider:LabeledSlider;
	private var mHorizontalFlipButton:Sprite;
	private var mVerticalFlipButton:Sprite;
	private var mModeSelector:DropDownSelector;
	
	private var mAnimation:Animation;
	private var mWidth:Float;
	private var mHeight:Float;

	public function new(startWidth:Float, startHeight:Float) 
	{
		super();
		
		mWidth = startWidth;
		mHeight = startHeight;
		
		mAnimationControlsText = new TextField();
		mAnimationControlsText.defaultTextFormat = new TextFormat(null, 16, 0x00FF00);
		mAnimationControlsText.mouseEnabled = false;
		mAnimationControlsText.background = true;
		mAnimationControlsText.backgroundColor = 0x003300;
		mAnimationControlsText.border = true;
		mAnimationControlsText.borderColor = 0x00FF00;
		mAnimationControlsText.width = startWidth * 0.8 - 1;
		mAnimationControlsText.height = startHeight * 0.11;
		mAnimationControlsText.text = "Animation Controls";
		
		mRotateSlider = LabeledSlider.makeDefaultLabeledSlider(0xFF7F7F7F, 0xFF999999, 0x333333, startWidth * 0.65, 
			startHeight * 0.02, startHeight * 0.1, "Rotate", startWidth * 0.25, startWidth * 0.15, 0, 1, 0.5, false, 0, 2, true,
			new TextFormat(null, 12, 0x999999));
		mScaleSlider = LabeledSlider.makeStyleLabeledSlider(new RampSliderStyle(), 0xFF7F7F7F, 0xFF999999, 0x333333, startWidth * 0.65, 
			startHeight * 0.02, startHeight * 0.1, "Scale", startWidth * 0.25, startWidth * 0.15, 0.1, 10, 1, false, 1, 2, true,
			new TextFormat(null, 12, 0x999999));
			
		mRedSlider = LabeledSlider.makeStyleLabeledSlider(new GradientSliderStyle(), 0xFFFF0000, 0xFF000000, 0x330000, startWidth * 0.65, 
			startHeight * 0.02, startHeight * 0.1, "Red", startWidth * 0.25, startWidth * 0.15, 0, 1, 0.5, false, 1, 2, true, 
			new TextFormat(null, 12, 0xFF0000));
		mGreenSlider = LabeledSlider.makeStyleLabeledSlider(new GradientSliderStyle(), 0xFF00FF00, 0xFF000000, 0x003300, startWidth * 0.65, 
			startHeight * 0.02, startHeight * 0.1, "Green", startWidth * 0.25, startWidth * 0.15, 0, 1, 0.5, false, 1, 2, true, 
			new TextFormat(null, 12, 0x00FF00));
		mBlueSlider = LabeledSlider.makeStyleLabeledSlider(new GradientSliderStyle(), 0xFF0000FF, 0xFF000000, 0x000033, startWidth * 0.65, 
			startHeight * 0.02, startHeight * 0.1, "Blue", startWidth * 0.25, startWidth * 0.15, 0, 1, 0.5, false, 1, 2, true, 
			new TextFormat(null, 12, 0x0000FF));
		mAlphaSlider = LabeledSlider.makeStyleLabeledSlider(new GradientSliderStyle(), 0xFFFFFFFF, 0xFF000000, 0x333333, startWidth * 0.65, 
			startHeight * 0.02, startHeight * 0.1, "Alpha", startWidth * 0.25, startWidth * 0.15, 0, 1, 0.5, false, 1, 2, true, 
			new TextFormat(null, 12, 0xFFFFFF));
			
		mSpeedSlider = LabeledSlider.makeStyleLabeledSlider(new RampSliderStyle(), 0xFFFFFF00, 0xFF999999, 0x333300, startWidth * 0.65, 
			startHeight * 0.02, startHeight * 0.1, "Speed", startWidth * 0.25, startWidth * 0.15, 0.1, 10, 1, false, 1, 2, true,
			new TextFormat(null, 12, 0x999999));
			
		mModeSelector = new DropDownSelector(startWidth * 0.9, startHeight * 0.08, startHeight * 0.6, 
			0xFFFF00, 0x999900, Type.getEnumConstructs(AnimationMode), 20, 12);
			
		addChild(mAnimationControlsText);
			
		addChild(mRotateSlider);
		addChild(mScaleSlider);
			
		addChild(mRedSlider);
		addChild(mGreenSlider);
		addChild(mBlueSlider);
		addChild(mAlphaSlider);
		
		addChild(mSpeedSlider);
		addChild(mModeSelector);
		
		mHorizontalFlipButton = new Sprite();
		mHorizontalFlipButton.useHandCursor = mHorizontalFlipButton.buttonMode = true;
		mHorizontalFlipButton.addEventListener(MouseEvent.CLICK, flipButtonListener);
		mVerticalFlipButton = new Sprite();
		mVerticalFlipButton.useHandCursor = mVerticalFlipButton.buttonMode = true;
		mVerticalFlipButton.addEventListener(MouseEvent.CLICK, flipButtonListener);
		
		addChild(mHorizontalFlipButton);
		addChild(mVerticalFlipButton);
		
		mRotateSlider.addEventListener(SliderEvent.ON_CHANGE, sliderListener);
		mScaleSlider.addEventListener(SliderEvent.ON_CHANGE, sliderListener);
		mRedSlider.addEventListener(SliderEvent.ON_CHANGE, sliderListener);
		mGreenSlider.addEventListener(SliderEvent.ON_CHANGE, sliderListener);
		mBlueSlider.addEventListener(SliderEvent.ON_CHANGE, sliderListener);
		mAlphaSlider.addEventListener(SliderEvent.ON_CHANGE, sliderListener);
		mSpeedSlider.addEventListener(SliderEvent.ON_CHANGE, sliderListener);
		mRotateSlider.addEventListener(SliderEvent.ON_PRESS, saveUndoStateListener);
		mScaleSlider.addEventListener(SliderEvent.ON_PRESS, saveUndoStateListener);
		mRedSlider.addEventListener(SliderEvent.ON_PRESS, saveUndoStateListener);
		mGreenSlider.addEventListener(SliderEvent.ON_PRESS, saveUndoStateListener);
		mBlueSlider.addEventListener(SliderEvent.ON_PRESS, saveUndoStateListener);
		mAlphaSlider.addEventListener(SliderEvent.ON_PRESS, saveUndoStateListener);
		mSpeedSlider.addEventListener(SliderEvent.ON_PRESS, saveUndoStateListener);
		mRotateSlider.addEventListener(NumberInput.VALUE_CHANGE, saveUndoStateListener);
		mScaleSlider.addEventListener(NumberInput.VALUE_CHANGE, saveUndoStateListener);
		mRedSlider.addEventListener(NumberInput.VALUE_CHANGE, saveUndoStateListener);
		mGreenSlider.addEventListener(NumberInput.VALUE_CHANGE, saveUndoStateListener);
		mBlueSlider.addEventListener(NumberInput.VALUE_CHANGE, saveUndoStateListener);
		mAlphaSlider.addEventListener(NumberInput.VALUE_CHANGE, saveUndoStateListener);
		mSpeedSlider.addEventListener(NumberInput.VALUE_CHANGE, saveUndoStateListener);
		mModeSelector.addEventListener(SelectorEvent.SELECTED, modeListener);
		
		resize(startWidth, startHeight);
		setAnimation();
	}
	
	public function setAnimation(?animation:Animation):Void
	{
		if (animation != mAnimation)
		{
			mAnimation = animation;
			
			updateControls();
		}
	}
	
	private function drawBack():Void
	{
		var active:Bool = mAnimation != null;
		
		mRotateSlider.visible = mScaleSlider.visible = mRedSlider.visible = 
			mGreenSlider.visible = mBlueSlider.visible = mAlphaSlider.visible = 
			mSpeedSlider.visible = mModeSelector.visible = 
			mHorizontalFlipButton.visible = mVerticalFlipButton.visible = active;
		
		graphics.clear();
		graphics.lineStyle(2, active ? 0x00FF00 : 0x336633);
		graphics.beginFill(active ? 0x336633 : 0x112211);
		graphics.drawRect(1, 1, mWidth - 2, mHeight - 2);
		mAnimationControlsText.borderColor = mAnimationControlsText.textColor = active ? 0x00FF00 : 0x336633;
	}
	
	public function resize(newWidth:Float, newHeight:Float):Void
	{
		mWidth = newWidth;
		mHeight = newHeight;
		drawBack();
		
		
		new DefaultSliderStyle().draw(mRotateSlider.track, mRotateSlider.head, newWidth * 0.55, 
			newHeight * 0.02, newHeight * 0.04, 0xFFCCCCCC, 0xFF999999, false);
		
		new RampSliderStyle().draw(mScaleSlider.track, mScaleSlider.head, newWidth * 0.55, 
			newHeight * 0.05, newHeight * 0.08, 0xFFCCCCCC, 0xFF999999, false);
		
		var style:ISliderStyle = new GradientSliderStyle();
		style.draw(mRedSlider.track, mRedSlider.head, newWidth * 0.55, newHeight * 0.02, newHeight * 0.1, 0xFFFF0000, 0xFF000000, false);
		style.draw(mGreenSlider.track, mGreenSlider.head, newWidth * 0.55, newHeight * 0.02, newHeight * 0.1, 0xFF00FF00, 0xFF000000, false);
		style.draw(mBlueSlider.track, mBlueSlider.head, newWidth * 0.55, newHeight * 0.02, newHeight * 0.1, 0xFF0000FF, 0xFF000000, false);
		style.draw(mAlphaSlider.track, mAlphaSlider.head, newWidth * 0.55, newHeight * 0.02, newHeight * 0.1, 0xFFFFFFFF, 0xFF000000, false);
		
		new RampSliderStyle().draw(mSpeedSlider.track, mSpeedSlider.head, newWidth * 0.55, 
			newHeight * 0.02, newHeight * 0.04, 0xFFFFFF00, 0xFF999999, false);
			
		mAnimationControlsText.width = newWidth * 0.8 - 1;
		mAnimationControlsText.height = newHeight * 0.11;
		
		drawFlipButtons(newWidth * 0.2, newHeight * 0.11);
		mHorizontalFlipButton.x = mAnimationControlsText.x + mAnimationControlsText.width;
		mVerticalFlipButton.x = mHorizontalFlipButton.x + mHorizontalFlipButton.width;
		
		mRotateSlider.label.width = newWidth * 0.25;	mRotateSlider.input.width = newWidth * 0.2 - 5;
		mScaleSlider.label.width = newWidth * 0.25;		mScaleSlider.input.width = newWidth * 0.2 - 5;
		mRedSlider.label.width = newWidth * 0.25;		mRedSlider.input.width = newWidth * 0.2 - 5;
		mGreenSlider.label.width = newWidth * 0.25;		mGreenSlider.input.width = newWidth * 0.2 - 5;
		mBlueSlider.label.width = newWidth * 0.25;		mBlueSlider.input.width = newWidth * 0.2 - 5;
		mAlphaSlider.label.width = newWidth * 0.25;		mAlphaSlider.input.width = newWidth * 0.2 - 5;
		mSpeedSlider.label.width = newWidth * 0.25;		mSpeedSlider.input.width = newWidth * 0.2 - 5;
		mModeSelector.width = newWidth * 0.9;
		
		mRotateSlider.x = 1;	mRotateSlider.y = newHeight * 0.11;
		mScaleSlider.x = 1;		mScaleSlider.y = newHeight * 0.23;
		
		mRedSlider.x = 1;		mRedSlider.y = newHeight * 0.34;
		mGreenSlider.x = 1;		mGreenSlider.y = newHeight * 0.45;
		mBlueSlider.x = 1;		mBlueSlider.y = newHeight * 0.56;
		mAlphaSlider.x = 1;		mAlphaSlider.y = newHeight * 0.67;
		
		mSpeedSlider.x = 1;		mSpeedSlider.y = newHeight * 0.78;
		
		mModeSelector.x = newWidth * 0.05;	mModeSelector.y = newHeight * 0.89;
		
		mRotateSlider.refresh();	mScaleSlider.refresh();
		mRedSlider.refresh();		mGreenSlider.refresh();		
		mBlueSlider.refresh();		mAlphaSlider.refresh();
		mSpeedSlider.refresh();
	}
	
	private function updateControls():Void
	{
		mRotateSlider.value = mAnimation.rotation;
		mScaleSlider.value = mAnimation.scale;
		mRedSlider.value = mAnimation.red;
		mGreenSlider.value = mAnimation.green;
		mBlueSlider.value = mAnimation.blue;
		mAlphaSlider.value = mAnimation.alpha;
		mSpeedSlider.value = mAnimation.animationSpeed;
		mModeSelector.setSelectedEntry(Std.string(mAnimation.animationMode));
		drawBack();
		drawFlipButtons();
	}
	
	private function flipButtonListener(e:MouseEvent):Void
	{
		dispatchEvent(new AnxEditEvent(AnxEditEvent.SAVE_UNDO_STATE));
		if (e.target == mHorizontalFlipButton)
			mAnimation.flipHorizontal = !mAnimation.flipHorizontal;
		else
			mAnimation.flipVertical = !mAnimation.flipVertical;
		drawFlipButtons();
	}
	
	private function drawFlipButtons(?bothWidth:Float, ?height:Float):Void
	{
		var w:Float = bothWidth != null ? bothWidth * 0.5 : mHorizontalFlipButton.width;
		var h:Float = height != null ? height : mHorizontalFlipButton.height;
		mHorizontalFlipButton.graphics.clear();
		mVerticalFlipButton.graphics.clear();
		//if (mAnimation == null)
		//	return;
		if (mAnimation!= null && mAnimation.flipHorizontal)
		{
			mHorizontalFlipButton.graphics.lineStyle(2, 0x00FF00, 1);
			mHorizontalFlipButton.graphics.beginFill(0x336633);
		}
		else
		{
			mHorizontalFlipButton.graphics.lineStyle(2, 0x336633, 1);
			mHorizontalFlipButton.graphics.beginFill(0x003300);
		}
		mHorizontalFlipButton.graphics.drawRect(1, 1, w - 2, h - 2);
		if (mAnimation!= null && mAnimation.flipHorizontal)
			mHorizontalFlipButton.graphics.lineStyle(2, 0xFFFFFF, 1);
			
		mHorizontalFlipButton.graphics.moveTo(w * 0.2, h * 0.5);
		mHorizontalFlipButton.graphics.lineTo(w * 0.3, h * 0.6);
		mHorizontalFlipButton.graphics.lineTo(w * 0.3, h * 0.4);
		mHorizontalFlipButton.graphics.lineTo(w * 0.2, h * 0.5);
		mHorizontalFlipButton.graphics.lineTo(w * 0.8, h * 0.5);
		mHorizontalFlipButton.graphics.lineTo(w * 0.7, h * 0.6);
		mHorizontalFlipButton.graphics.lineTo(w * 0.7, h * 0.4);
		mHorizontalFlipButton.graphics.lineTo(w * 0.8, h * 0.5);
		if (mAnimation!= null && mAnimation.flipVertical)
		{
			mVerticalFlipButton.graphics.lineStyle(2, 0x00FF00, 1);
			mVerticalFlipButton.graphics.beginFill(0x336633);
		}
		else
		{
			mVerticalFlipButton.graphics.lineStyle(2, 0x336633, 1);
			mVerticalFlipButton.graphics.beginFill(0x003300);
		}
		mVerticalFlipButton.graphics.drawRect(1, 1, w - 2, h - 2);
		if (mAnimation!= null && mAnimation.flipVertical)
			mVerticalFlipButton.graphics.lineStyle(2, 0xFFFFFF, 1);
			
		mVerticalFlipButton.graphics.moveTo(w * 0.5, h * 0.2);
		mVerticalFlipButton.graphics.lineTo(w * 0.6, h * 0.3);
		mVerticalFlipButton.graphics.lineTo(w * 0.4, h * 0.3);
		mVerticalFlipButton.graphics.lineTo(w * 0.5, h * 0.2);
		mVerticalFlipButton.graphics.lineTo(w * 0.5, h * 0.8);
		mVerticalFlipButton.graphics.lineTo(w * 0.6, h * 0.7);
		mVerticalFlipButton.graphics.lineTo(w * 0.4, h * 0.7);
		mVerticalFlipButton.graphics.lineTo(w * 0.5, h * 0.8);
	}
	
	private function sliderListener(e:SliderEvent):Void
	{
		if (mAnimation == null)
			return;
		if (e.source == mRotateSlider.slider)
		{
			mAnimation.rotation = mRotateSlider.value;
			mRotateSlider.value = mAnimation.rotation;
		}
		else if (e.source == mScaleSlider.slider)
		{
			mAnimation.scale = mScaleSlider.value;
			mScaleSlider.value = mAnimation.scale;
		}
		else if (e.source == mRedSlider.slider)
		{
			mAnimation.red = mRedSlider.value;
			mRedSlider.value = mAnimation.red;
		}
		else if (e.source == mGreenSlider.slider)
		{
			mAnimation.green = mGreenSlider.value;
			mGreenSlider.value = mAnimation.green;
		}
		else if (e.source == mBlueSlider.slider)
		{
			mAnimation.blue = mBlueSlider.value;
			mBlueSlider.value = mAnimation.blue;
		}
		else if (e.source == mAlphaSlider.slider)
		{
			mAnimation.alpha = mAlphaSlider.value;
			mAlphaSlider.value = mAnimation.alpha;
		}
		else if (e.source == mSpeedSlider.slider)
		{
			mAnimation.animationSpeed = mSpeedSlider.value;
			mSpeedSlider.value = mAnimation.animationSpeed;
		}
		else
			throw new Error("bad slider");
	}
	
	private inline function saveUndoStateListener(e:Event):Void
	{
		dispatchEvent(new AnxEditEvent(AnxEditEvent.SAVE_UNDO_STATE));
	}
	
	private function modeListener(e:SelectorEvent):Void
	{
		var mode = Type.createEnum(AnimationMode, e.item);
		if (mode != mAnimation.animationMode)
		{
			dispatchEvent(new AnxEditEvent(AnxEditEvent.SAVE_UNDO_STATE));
			mAnimation.animationMode = mode;
		}
	}
}