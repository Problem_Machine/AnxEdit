package com.problemmachine.anxedit;

import com.problemmachine.animation.Animation;
import com.problemmachine.animation.Frame;
import com.problemmachine.animation.event.AnimationEvent;
import com.problemmachine.anxedit.controls.AnxAnimationControls;
import com.problemmachine.anxedit.controls.AnxFileControls;
import com.problemmachine.anxedit.controls.AnxFrameControls;
import com.problemmachine.anxedit.controls.AnxFramerateControls;
import com.problemmachine.anxedit.controls.AnxOffsetControls;
import com.problemmachine.anxedit.controls.AnxRegPointControls;
import com.problemmachine.anxedit.event.AnxEditEvent;
import com.problemmachine.anxedit.image.AnxFrameImageEditor;
import com.problemmachine.anxedit.image.AnxImageImporter;
import com.problemmachine.anxedit.image.AnxImageRecompiler;
import com.problemmachine.anxedit.timeline.AnxTimeline;
import com.problemmachine.bitmap.bitmapmanager.BitmapManager;
import com.problemmachine.tools.file.FileTools;
import com.problemmachine.tools.keyboard.KeyboardTools;
import com.problemmachine.tools.text.TextTools;
import com.problemmachine.ui.ConfirmationWindow;
import flash.Lib;
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.FocusEvent;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.events.TimerEvent;
import flash.filesystem.File;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.net.FileFilter;
import flash.net.SharedObject;
import flash.text.TextField;
import flash.text.TextFormat;
import flash.text.TextFormatAlign;
import flash.ui.Keyboard;
import flash.utils.Timer;

class AnxEdit extends Sprite
{
	private var mInitted:Bool;
	
	private var mAnimationControls:AnxAnimationControls;
	private var mFrameControls:AnxFrameControls;
	private var mOffsetControls:AnxOffsetControls;
	private var mRegPointControls:AnxRegPointControls;
	private var mFileControls:AnxFileControls;
	private var mTimeline:AnxTimeline;
	private var mFramerateControls:AnxFramerateControls;
	
	private var mMouseBlockerOverlay:Sprite;
	private var mFrameImageEditor:AnxFrameImageEditor;
	private var mImageImporter:AnxImageImporter;
	private var mImageRecompiler:AnxImageRecompiler;
	private var mRegPointNodes:Array<RegistrationPointNode>;
	private var mRegPointNodeContainer:Sprite;
	private var mDraggingRegPoint:RegistrationPointNode;
	
	private var mCamera:Point;
	private var mZoom:Float;
	private var mDraggingCamera:Point;
	private var mEditAnimation:Animation;
	private var mDrawSurfaceContainer:Sprite;
	private var mDrawSurface:Bitmap;
	private var mDrawOverlay:Sprite;
	private var mCurrentPath:String;
	private var mErrorDisplay:TextField;
	private var mRootDirectorySelector:Sprite;
	private var mRootDirectorySelectorText:TextField;
	private var mAbsolutePathToggle:Sprite;
	
	private var mAnimationPlaying:Bool;
	private var mLastUpdateTime:Int;
	private var mTimer:Timer;
	
	private var mUndoStack:Array<Xml>;
	private var mRedoStack:Array<Xml>;
	
	private var mConfirmWindow:ConfirmationWindow;

	public function new(startWidth:Float, startHeight:Float) 
	{
		super();
		
		mInitted = false;
		
		KeyboardTools.startListeningForKeyDown(Keyboard.DELETE, keyListener);
		KeyboardTools.startListeningForKeyDown(Keyboard.SPACE, keyListener);
		KeyboardTools.startListeningForKeyDown(Keyboard.Z, keyListener);
		KeyboardTools.startListeningForKeyDown(Keyboard.Y, keyListener);
		KeyboardTools.startListeningForKeyDown(Keyboard.RIGHT, keyListener);
		KeyboardTools.startListeningForKeyDown(Keyboard.LEFT, keyListener);
		
		mDrawSurfaceContainer = new Sprite();
		mDrawSurface = new Bitmap(new BitmapData(Math.ceil(startWidth * 0.8), Math.ceil(startHeight * 0.75), false, 0x000000));
		mDrawSurfaceContainer.addEventListener(MouseEvent.MOUSE_DOWN, surfaceMouseListener);
		mDrawSurfaceContainer.addChild(mDrawSurface);
		addChild(mDrawSurfaceContainer);
		
		mRegPointNodes = [];
		mRegPointNodeContainer = new Sprite();
		addChild(mRegPointNodeContainer);
		
		mFileControls = new AnxFileControls(startWidth, startHeight * 0.05);
		mAnimationControls = new AnxAnimationControls(startWidth * 0.2, startHeight * 0.3);
		mFrameControls = new AnxFrameControls(startWidth * 0.2, startHeight * 0.25);
		mOffsetControls = new AnxOffsetControls(startWidth * 0.2, startHeight * 0.2);
		mRegPointControls = new AnxRegPointControls(startWidth * 0.1, startHeight * 0.75);
		mRegPointControls.addEventListener(Event.CHANGE, updateRegPoints);
		
		mFrameImageEditor = new AnxFrameImageEditor(startWidth * 0.8, startHeight * 0.8);
		mImageImporter = new AnxImageImporter(startWidth * 0.8, startHeight * 0.8);
		mImageRecompiler = new AnxImageRecompiler(startWidth * 0.8, startHeight * 0.8);
		
		mTimeline = new AnxTimeline(startWidth - startHeight * 0.2, startHeight * 0.2);
		mTimeline.y = startHeight * 0.8;
		mFramerateControls = new AnxFramerateControls(startHeight * 0.2, startHeight * 0.2);
		mFramerateControls.x = mTimeline.width;
		mFramerateControls.y = mTimeline.y;
		mDrawOverlay = new Sprite();
		
		addChild(mFileControls);
		addChild(mFrameControls);
		addChild(mAnimationControls);
		addChild(mOffsetControls);
		addChild(mRegPointControls.panel);
		addChild(mTimeline);
		addChild(mFramerateControls);
		
		mTimeline.addEventListener(AnxTimeline.SELECTION_CHANGED, selectFramesListener);
		mTimeline.addEventListener(AnxTimeline.EDIT_FRAME_IMAGE, editFrameImage);
		mTimeline.addEventListener(AnxEditEvent.IMPORT_IMAGE, importImageListener);
		mFramerateControls.addEventListener(AnxEditEvent.SET_FRAMERATE, setFramerateListener);
		
		mFileControls.addEventListener(AnxFileControls.NEW_EVENT, newListener);
		mFileControls.addEventListener(AnxFileControls.SAVE_EVENT, saveListener);
		mFileControls.addEventListener(AnxFileControls.SAVE_AS_EVENT, saveAsListener);
		mFileControls.addEventListener(AnxFileControls.LOAD_EVENT, loadListener);
		mFileControls.addEventListener(AnxFileControls.RECOMPILE_EVENT, recompileListener);
		
		mAnimationControls.addEventListener(AnxEditEvent.SAVE_UNDO_STATE, saveUndoState);
		mFrameControls.addEventListener(AnxEditEvent.SAVE_UNDO_STATE, saveUndoState);
		mOffsetControls.addEventListener(AnxEditEvent.SAVE_UNDO_STATE, saveUndoState);
		mRegPointControls.addEventListener(AnxEditEvent.SAVE_UNDO_STATE, saveUndoState);
		mTimeline.addEventListener(AnxEditEvent.SAVE_UNDO_STATE, saveUndoState);
		
		mErrorDisplay = new TextField();
		mErrorDisplay.defaultTextFormat = new TextFormat(null, 16, 0xFF0000);
		mErrorDisplay.mouseEnabled = false;
		mErrorDisplay.height = 20;
		addChild(mErrorDisplay);
		
		var root:String = cast SharedObject.getLocal("root").data.path;
		if (root == null)
			root = File.applicationDirectory.nativePath;
		BitmapManager.rootDirectory = root;
		mRootDirectorySelector = new Sprite();
		mRootDirectorySelector.buttonMode = mRootDirectorySelector.useHandCursor = true;
		mRootDirectorySelector.alpha = 0.6;
		mRootDirectorySelectorText = new TextField();
		mRootDirectorySelectorText.defaultTextFormat = new TextFormat("Consolas", 14, 0xFFFFFF);
		mRootDirectorySelectorText.text = BitmapManager.rootDirectory;
		mRootDirectorySelectorText.mouseEnabled = false;
		mRootDirectorySelector.addChild(mRootDirectorySelectorText);
		mRootDirectorySelectorText.x = 10;
		mRootDirectorySelectorText.height = 20;
		addChild(mRootDirectorySelector);
		mRootDirectorySelector.addEventListener(MouseEvent.CLICK, setRootDirectoryListener);
		
		mMouseBlockerOverlay = new Sprite();
		
		mCamera = new Point();
		mZoom = 1;
		mDraggingCamera = null;
		
		addEventListener(Event.ADDED_TO_STAGE, stageListener);
		addEventListener(FocusEvent.FOCUS_OUT, focusListener);
		
		mTimer = new Timer(1000 / 60);
		mTimer.addEventListener(TimerEvent.TIMER, update);
		mTimer.start();
		mLastUpdateTime = Lib.getTimer();
		
		mConfirmWindow = null;
	}
	
	private function setAnimation(animation:Animation, resetUndo:Bool = true):Void
	{
		if (resetUndo)
		{
			mUndoStack = [];
			mRedoStack = [];
		}
		if (mEditAnimation != animation && mEditAnimation != null)
			mEditAnimation.removeEventListener(AnimationEvent.ANIM_FINISH, animationListener);
		mEditAnimation = animation;
		if (mEditAnimation != null)
			mEditAnimation.addEventListener(AnimationEvent.ANIM_FINISH, animationListener);
		mTimeline.setAnimation(animation);
		mAnimationControls.setAnimation(animation);
		mFrameControls.setAnimation(animation);
		mOffsetControls.setAnimation(animation);
		mRegPointControls.setAnimation(animation);
		mFramerateControls.setButtonsVisible(animation != null && animation.numFrames > 0, false);
	}
	
	private function animationListener(e:AnimationEvent):Void
	{
		mAnimationPlaying = false;
		mEditAnimation.reset();
	}
	
	private function update(e:TimerEvent):Void
	{
		var time:Float = (Lib.getTimer() - mLastUpdateTime) / 1000;
		mLastUpdateTime = Lib.getTimer();
		
		var color1:UInt = 0xFF000000 | (Std.int((Math.sin(Lib.getTimer() * Math.PI * 2 / 5000) * 0.5 + 0.5) * 0xFF) << 16);
		var color2:UInt = 0xFF000000 | (Std.int((Math.sin((Lib.getTimer() * Math.PI * 2 / 5000) + Math.PI) * 0.5 + 0.5) * 0xFF));
		mDrawSurface.bitmapData.fillRect(new Rectangle(0, 0, mDrawSurface.bitmapData.width, mDrawSurface.bitmapData.height), color1);
		for (x in 0...Math.ceil(Math.max(mDrawSurface.bitmapData.width, mDrawSurface.bitmapData.height) / 20))
			for (y in 0...Math.ceil(Math.max(mDrawSurface.bitmapData.width, mDrawSurface.bitmapData.height) / 20))
				if ((x + y) % 2 != 0)
					mDrawSurface.bitmapData.fillRect(new Rectangle(x * 20, y * 20, 20, 20), color2);
		mRegPointNodeContainer.x = mDrawSurface.x + mCamera.x;
		mRegPointNodeContainer.y = mDrawSurface.y + mCamera.y;
		if (mEditAnimation != null)
		{
			var tmp:Float = mEditAnimation.scale;
			if (mAnimationPlaying)
				mEditAnimation.progress(time);
			else if (mTimeline.selectedFrames.length > 0)
				mEditAnimation.currentFrame = mTimeline.selectedFrames[0];
			mEditAnimation.scale = mEditAnimation.scale * mZoom;
			mEditAnimation.draw(mDrawSurface.bitmapData, new Point(mCamera.x, mCamera.y));
			mEditAnimation.scale = tmp;
			mDrawSurface.bitmapData.fillRect(new Rectangle(mCamera.x - 1, mCamera.y - 1, 2, 2), 0xFFFFFFFF);
		}
	}
	
	private function saveUndoState(?e:AnxEditEvent):Void
	{
		mRedoStack = [];
		mUndoStack.push(mEditAnimation.toXML());
	}
	private function undo():Void
	{
		if (mUndoStack.length <= 0)
			return;
		var x:Xml = mUndoStack.pop();
		mRedoStack.push(mEditAnimation.toXML());
		mEditAnimation.dispose();
		setAnimation(Animation.createFromXML(x, false), false);
	}
	private function redo():Void
	{
		if (mRedoStack.length <= 0)
			return;
		var x:Xml = mRedoStack.pop();
		mUndoStack.push(mEditAnimation.toXML());
		mEditAnimation.dispose();
		setAnimation(Animation.createFromXML(x, false), false);
	}
	
	private function selectFramesListener(e:Event):Void
	{
		mFrameControls.setSelectedFrames(mTimeline.selectedFrames);
		mOffsetControls.setSelectedFrames(mTimeline.selectedFrames);
		mRegPointControls.setSelectedFrames(mTimeline.selectedFrames);
		mFramerateControls.setButtonsVisible(mEditAnimation != null && mEditAnimation.numFrames > 0, mTimeline.selectedFrames.length > 1);
		updateRegPoints();
	}
	
	private function setFramerateListener(e:AnxEditEvent):Void
	{
		saveUndoState();
		var rate:Float = cast e.data;
		if (rate == 0)
			rate = mEditAnimation.getFrame(mTimeline.selectedFrames[0]).delay;
		if (mTimeline.selectedFrames.length > 0)
			for (i in mTimeline.selectedFrames)
				mEditAnimation.getFrame(i).delay = rate;
		else
			for (i in 0...mEditAnimation.numFrames)
				mEditAnimation.getFrame(i).delay = rate;
		mTimeline.refreshFramerate();
	}
	
	private function recompileListener(e:Event):Void
	{
		addChild(mMouseBlockerOverlay);
		addChild(mImageRecompiler);
		mImageRecompiler.beginRecompile(mEditAnimation, mCurrentPath);
		mImageRecompiler.addEventListener(Event.COMPLETE, completeRecompile);
		mImageRecompiler.addEventListener(Event.CLOSE, cancelRecompile);
	}
	private function completeRecompile(e:Event):Void
	{
		setAnimation(mImageRecompiler.getUpdatedAnimation());
		cancelRecompile();
	}
	private function cancelRecompile(?e:Event):Void
	{
		mImageRecompiler.endRecompile();
		removeChild(mMouseBlockerOverlay);
		if (mImageRecompiler.parent != null)
			mImageRecompiler.parent.removeChild(mImageRecompiler);
		mImageRecompiler.removeEventListener(Event.COMPLETE, completeRecompile);
		mImageRecompiler.removeEventListener(Event.CLOSE, cancelRecompile);
	}
	
	private function editFrameImage(e:Event):Void
	{
		addChild(mMouseBlockerOverlay);
		addChild(mFrameImageEditor);
		mFrameImageEditor.setFrame(mEditAnimation.getFrame(mTimeline.selectedFrames[0]));
		mFrameImageEditor.addEventListener(Event.CLOSE, stopEditingFrameImage);
	}
	private function stopEditingFrameImage(e:Event):Void
	{
		removeChild(mMouseBlockerOverlay);
		if (mFrameImageEditor.parent != null)
			mFrameImageEditor.parent.removeChild(mFrameImageEditor);
		mFrameImageEditor.finalizeFrame(mEditAnimation.getFrame(mTimeline.selectedFrames[0]));
		mFrameImageEditor.removeEventListener(Event.CLOSE, stopEditingFrameImage);
		mFrameImageEditor.path = null;
		mTimeline.refreshImage();
	}
	
	private function importImageListener(e:AnxEditEvent):Void
	{
		addChild(mMouseBlockerOverlay);
		addChild(mImageImporter);
		mImageImporter.setImage(cast e.data);
		mImageImporter.addEventListener(Event.COMPLETE, completeImageImport);
		mImageImporter.addEventListener(Event.CLOSE, cancelImageImport);
	}
	private function completeImageImport(e:Event):Void
	{
		saveUndoState();
		var frames:Array<Frame> = mImageImporter.getFrames();
		for (f in frames)
			mEditAnimation.addRawFrame(f);
		mImageImporter.setImage();
		mTimeline.redisplay();
		cancelImageImport();
	}
	private function cancelImageImport(?e:Event):Void
	{
		removeChild(mMouseBlockerOverlay);
		if (mImageImporter.parent != null)
			mImageImporter.parent.removeChild(mImageImporter);
		mImageImporter.removeEventListener(Event.COMPLETE, completeImageImport);
		mImageImporter.removeEventListener(Event.CLOSE, cancelImageImport);
	}
	
	@:access(com.problemmachine.animation.Frame.mRegistrationPoints)
	private function updateRegPoints(?e:Event):Void
	{
		while (mRegPointNodes.length > 0)
		{
			var s:RegistrationPointNode = mRegPointNodes.pop();
			if (s.parent != null)
				mRegPointNodeContainer.removeChild(s);
			s.removeEventListener(MouseEvent.MOUSE_DOWN, dragRegPoint);
		}
		
		if (mTimeline.selectedFrames.length <= 0)
			return;
		var points:Array<Point> = mEditAnimation.getFrame(mTimeline.selectedFrames[0]).mRegistrationPoints;
		for (i in 0...points.length)
		{
			var rel:Array<Point> = [];
			var avg:Point = new Point();
			for (j in mTimeline.selectedFrames)
			{
				var p:Point = mEditAnimation.getFrame(j).mRegistrationPoints[i];
				avg.x += p.x;
				avg.y += p.y;
				rel.push(p.clone());
			}
			avg.x /= mTimeline.selectedFrames.length;
			avg.y /= mTimeline.selectedFrames.length;
			for (p in rel)
			{
				p.x -= avg.x;
				p.y -= avg.y;
			}
			var s:RegistrationPointNode = new RegistrationPointNode(14, 14, 0x00FF00, 0x114411, i, rel);
			s.x = avg.x;
			s.y = avg.y;
			mRegPointNodeContainer.addChild(s);
			mRegPointNodes.push(s);
			s.addEventListener(MouseEvent.MOUSE_DOWN, dragRegPoint);
		}
	}
	private function dragRegPoint(e:MouseEvent):Void
	{
		mDraggingRegPoint = cast e.target;
		mDraggingRegPoint.startDrag();
	}
	
	private function focusListener(e:Event):Void
	{
		if (mDraggingCamera != null)
		{
			mDraggingCamera = null;
			mDrawSurface.removeEventListener(MouseEvent.MOUSE_MOVE, surfaceMouseListener);
		}
	}
	
	private function stageListener(e:Event):Void
	{
		if (!mInitted)
		{
			stage.addEventListener(Event.RESIZE, stageListener);
			stage.addEventListener(MouseEvent.MOUSE_UP, surfaceMouseListener);
			stage.addEventListener(MouseEvent.MOUSE_WHEEL, zoomListener);
			stage.addEventListener(MouseEvent.MIDDLE_CLICK, zoomListener);
		}
		resize(stage.stageWidth, stage.stageHeight);
	}
	
	private function resize(newWidth:Float, newHeight:Float):Void
	{
		mMouseBlockerOverlay.graphics.clear();
		mMouseBlockerOverlay.graphics.beginFill(0x000000, 0.5);
		mMouseBlockerOverlay.graphics.drawRect(0, 0, newWidth, newHeight);
		mFileControls.resize(newWidth, newHeight * 0.05);
		mAnimationControls.resize(newWidth * 0.2, newHeight * 0.3);
		mFrameControls.resize(newWidth * 0.2, newHeight * 0.25);
		mOffsetControls.resize(newWidth * 0.2, newHeight * 0.2);
		mTimeline.resize(newWidth - newHeight * 0.2, newHeight * 0.2);
		mFramerateControls.resize(newHeight * 0.2, newHeight * 0.2);
		mRegPointControls.resize(newWidth * 0.1, newHeight * 0.75);
		mErrorDisplay.width = newWidth * 0.8;
		mRootDirectorySelector.graphics.clear();
		mRootDirectorySelector.graphics.lineStyle(4, 0xFFFFFF);
		mRootDirectorySelector.graphics.beginFill(0x333333);
		mRootDirectorySelector.graphics.drawRect(2, 2, newWidth * 0.7 - 2, 20);
		mRootDirectorySelectorText.width = mRootDirectorySelector.width - 8;
		
		mFrameImageEditor.resize(newWidth * 0.8, newHeight * 0.8);
		mImageImporter.resize(newWidth * 0.8, newHeight * 0.8);
		mImageRecompiler.resize(newWidth * 0.8, newHeight * 0.8);
		
		mDrawSurface.bitmapData.dispose();
		mDrawSurface.bitmapData = new BitmapData(Math.ceil(newWidth * 0.8), Math.ceil(newHeight * 0.75), false, 0x000000);
		
		mFileControls.x = 0; mFileControls.y = 0;
		mAnimationControls.x = 0; mAnimationControls.y = mFileControls.height;
		mFrameControls.x = 0;		mFrameControls.y = mAnimationControls.y + mAnimationControls.height;
		mOffsetControls.x = 0;		mOffsetControls.y = mFrameControls.y + mFrameControls.height;
		mTimeline.x = 0; mTimeline.y = mOffsetControls.y + mOffsetControls.height;
		mFramerateControls.x = mTimeline.width; mFramerateControls.y = mTimeline.y;
		mErrorDisplay.x = newHeight * 0.1;	mErrorDisplay.y = newHeight * 0.8 - 20;
		mRootDirectorySelector.x = mAnimationControls.width;	mRootDirectorySelector.y = mFileControls.height;
		mDrawSurface.x = mRootDirectorySelector.x; mDrawSurface.y = mRootDirectorySelector.y;
		mRegPointControls.x = newWidth * 0.9;	mRegPointControls.y = mFileControls.height;
		
		mFrameImageEditor.x = newWidth * 0.1;	mFrameImageEditor.y = newHeight * 0.1;
		mImageImporter.x = newWidth * 0.1;	mImageImporter.y = newHeight * 0.1;
		mImageRecompiler.x = newWidth * 0.1;	mImageRecompiler.y = newHeight * 0.1;
	}
	
	private function activateConfirmWindow():Void
	{
		addChild(mMouseBlockerOverlay);
		addChild(mConfirmWindow);
	}
	
	private function cancelConfirmWindow():Void
	{
		if (mMouseBlockerOverlay.parent != null)
			mMouseBlockerOverlay.parent.removeChild(mMouseBlockerOverlay);
		if (mConfirmWindow != null)
			mConfirmWindow.parent.removeChild(mConfirmWindow);
		mConfirmWindow = null;
	}
	
	private function setRootDirectoryListener(e:Event):Void
	{
		var file:File = File.applicationStorageDirectory.resolvePath(BitmapManager.rootDirectory); 
		file.addEventListener(Event.SELECT, completeSetRootDirectoryListener);
		file.browseForDirectory("SetRootDirectory");
	}
	private function completeSetRootDirectoryListener(e:Event):Void
	{
		mRootDirectorySelectorText.text = cast(e.target, File).nativePath + "\\";
		TextTools.scaleTextToFit(mRootDirectorySelectorText);
		cast(e.target, File).removeEventListener(Event.SELECT, completeSetRootDirectoryListener);
		if (mEditAnimation != null)
		{
			mConfirmWindow = new ConfirmationWindow(width * 0.8, height * 0.8, 
				"Update animation pathing \nto match new root path?\n Animation may not load\n properly if pathing\n is not updated.",
				["Update", "Don't Update"], [setAnimationRootPath, finishSetRootDirectory]);
			activateConfirmWindow();
		}
		else
			finishSetRootDirectory();
	}
	private function setAnimationRootPath():Void
	{
		saveUndoState();
		var paths:Array<String> = [];
		for (i in 0...mEditAnimation.numFrames)
		{
			paths.push(BitmapManager.rootDirectory + mEditAnimation.getFrame(i).resource);
			mEditAnimation.getFrame(i).resource = "";
		}
		finishSetRootDirectory();
		for (i in 0...paths.length)
			mEditAnimation.getFrame(i).resource = new File(FileTools.formatPath(BitmapManager.rootDirectory)).getRelativePath(new File(paths[i]));
	}
	private function finishSetRootDirectory():Void
	{
		var root:SharedObject = SharedObject.getLocal("root");
		root.data.path = BitmapManager.rootDirectory = mRootDirectorySelectorText.text;
		root.flush();
		cancelConfirmWindow();
	}
	
	private function newListener(e:Event):Void
	{
		if (mCurrentPath != null)
			mCurrentPath = FileTools.directoryOf(mCurrentPath) + "animation.anx";
		else
			mCurrentPath = BitmapManager.rootDirectory + "animation.anx";
		setAnimation(new Animation(false, ForwardLoop, true));
	}
	
	private function saveListener(?e:Event):Void
	{
		if (mCurrentPath != null)
		{
			var success:Bool = 
				FileTools.immediateWriteTo(mCurrentPath, mEditAnimation.toXML());
			if (!success)
				trace ("Couldn't save animation at " + mCurrentPath);
		}
		else
			saveAsListener();
	}
	
	private function saveAsListener(?e:Event):Void
	{
		var file:File = 
			mCurrentPath != null ? new File(mCurrentPath) : 
			File.applicationStorageDirectory.resolvePath(BitmapManager.rootDirectory + "animation.anx"); 
		file.canonicalize();
		file.addEventListener(Event.SELECT, completeSaveAsListener);
		file.browseForSave("Save Animation File As");
	}
	private function completeSaveAsListener(e:Event):Void
	{
		mCurrentPath = cast(e.target, File).nativePath;
		saveListener(e);
		cast(e.target, File).removeEventListener(Event.SELECT, completeSaveAsListener);
	}
	
	private function loadListener(e:Event):Void
	{
		var file:File = File.applicationStorageDirectory.resolvePath(BitmapManager.rootDirectory); 
		file.addEventListener(Event.SELECT, completeLoadListener);
		file.browseForOpen("Load Animation File", [new FileFilter("Animation XML File", "*.anx;*.xml")]);
	}
	
	private function completeLoadListener(e:Event):Void
	{
		e.target.removeEventListener(Event.SELECT, completeLoadListener);
		mCurrentPath = cast(e.target, File).nativePath;
		setAnimation(Animation.createFromPath(cast(e.target, File).nativePath, false));
		cast(e.target, File).removeEventListener(Event.SELECT, completeLoadListener);
	}
	
	@:access(com.problemmachine.animation.Frame.mRegistrationPoints)
	private function surfaceMouseListener(e:MouseEvent):Void
	{
		if (e.type == MouseEvent.MOUSE_DOWN)
		{
			mDraggingCamera = new Point(e.stageX, e.stageY);
			stage.addEventListener(MouseEvent.MOUSE_MOVE, surfaceMouseListener);
		}
		else if (e.type == MouseEvent.MOUSE_MOVE)
		{
			mCamera.x += e.stageX - mDraggingCamera.x;
			mCamera.y += e.stageY - mDraggingCamera.y;
			mDraggingCamera.x = e.stageX;
			mDraggingCamera.y = e.stageY;
		}
		else
		{
			if (mDraggingRegPoint != null)
			{
				saveUndoState();
				mDraggingRegPoint.stopDrag();
				var num:Int = Std.parseInt(mDraggingRegPoint.number.text);
				var p:Point = new Point(mDraggingRegPoint.x, mDraggingRegPoint.y);
				p = p.subtract(mEditAnimation.getFrame(mTimeline.selectedFrames[0]).mRegistrationPoints[num]);
				for (i in mTimeline.selectedFrames)
				{
					mEditAnimation.getFrame(i).mRegistrationPoints[num].x += p.x;
					mEditAnimation.getFrame(i).mRegistrationPoints[num].y += p.y;
				}
				mDraggingRegPoint = null;
				mRegPointControls.refresh();
			}
			mDraggingCamera = null;
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, surfaceMouseListener);
		}
	}
	
	private function zoomListener(e:MouseEvent):Void
	{
		if (e.type == MouseEvent.MOUSE_WHEEL)
		{
			if (e.delta > 0)
				mZoom *= 2;
			else
				mZoom *= 0.5;
			if (mZoom < 0.125)
				mZoom = 0.125;
			else if (mZoom > 8)
				mZoom = 8;
			else if (Math.abs(mZoom - 1) < 0.1)	// avoid precision drift
				mZoom = 1;
		}
		else
			mZoom = 1;
	}
	
	private function keyListener(e:KeyboardEvent):Void
	{
		switch(e.keyCode)
		{
			case Keyboard.DELETE:
				// TODO: confirm window
				if (!Std.is(Lib.current.stage.focus, TextField))
					mTimeline.deleteSelectedFrames();
			case Keyboard.SPACE:
				mAnimationPlaying = !mAnimationPlaying;
			case Keyboard.S:
				if (e.ctrlKey)
					saveListener();
			case Keyboard.A:
				if (e.ctrlKey)
					saveAsListener();
			case Keyboard.Z:
				if (e.ctrlKey)
					undo();
			case Keyboard.Y:
				if (e.ctrlKey)
					redo();
			case Keyboard.RIGHT:
				if (mEditAnimation == null || mEditAnimation.numFrames == 0)
					return;
				if (mTimeline.selectedFrames.length > 0)
				{
					var i:Int = mTimeline.selectedFrames[0] + 1;
					i %= mEditAnimation.numFrames;
					mTimeline.selectedFrames = [i];
				}
				else
					mTimeline.selectedFrames = [0];
			case Keyboard.LEFT:
				if (mEditAnimation == null || mEditAnimation.numFrames == 0)
					return;
				if (mTimeline.selectedFrames.length > 0)
				{
					var i:Int = mTimeline.selectedFrames[0] + 1;
					if (i < 0)
						i += mEditAnimation.numFrames;
					mTimeline.selectedFrames = [i];
				}
				else
					mTimeline.selectedFrames = [mEditAnimation.numFrames - 1];
		}
	}	
}

private class RegistrationPointNode extends Sprite
{
	public var number:TextField;
	
	public function new(width:Float, height:Float, color1:UInt, color2:UInt, id:Int, points:Array<Point>)
	{
		super();
		useHandCursor = buttonMode = true;
		number = new TextField();
		number.defaultTextFormat = new TextFormat("Consolas", 10, color1, null, null, null, null, null, TextFormatAlign.CENTER);
		number.mouseEnabled = false;
		number.width = width;
		number.height = height;
		number.border = number.background = true;
		number.borderColor = color1;
		number.backgroundColor = color2;
		number.text = Std.string(id);
		graphics.lineStyle(1, color1, 0.3);
		for (p in points)
		{
			graphics.moveTo(0, 0);
			graphics.lineTo(p.x, p.y);
		}
		graphics.lineStyle(1, color1);
		graphics.beginFill(color2);
		graphics.drawCircle(0, 0, 2);
		
		addChild(number);
	}
}