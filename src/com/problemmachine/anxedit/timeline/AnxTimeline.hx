package com.problemmachine.anxedit.timeline;
import com.problemmachine.animation.Animation;
import com.problemmachine.animation.event.AnimationEvent;
import com.problemmachine.animation.Frame;
import com.problemmachine.anxedit.event.AnxEditEvent;
import com.problemmachine.anxedit.timeline.frame.AnxTimelineFrame;
import com.problemmachine.bitmap.bitmapmanager.BitmapManager;
import com.problemmachine.tools.file.FileTools;
import com.problemmachine.tools.math.IntMath;
import com.problemmachine.ui.LabeledButton;
import com.problemmachine.ui.scrollwindow.ScrollWindow;
import flash.display.Sprite;
import flash.errors.Error;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.filesystem.File;
import flash.geom.Rectangle;
import flash.net.FileFilter;

class AnxTimeline extends Sprite
{
	static public inline var SELECTION_CHANGED:String = "SelectFramesEvent";
	static public inline var EDIT_FRAME_IMAGE:String = "EditFrameImageEvent";
	
	private var mAnimation:Animation;
	private var mTimelineScrollWindow:ScrollWindow;
	private var mTimelineFrames:Array<AnxTimelineFrame>;
	
	private var mAddBlankFrameButton:LabeledButton;
	private var mImportButton:LabeledButton;
	private var mCopyFrameButton:LabeledButton;
	private var mDeleteFrameButton:LabeledButton;
	
	public var selectedFrames(default, set):Array<Int>;

	public function new(startWidth:Float, startHeight:Float) 
	{
		super();
		
		mTimelineScrollWindow = new ScrollWindow(startWidth * 0.8, startHeight, 0x888888, 0x111111, 15);
		//super(startWidth, startHeight, 0x888888, 0x111111, 15);
		mTimelineScrollWindow.enableHorizontalResize = false;
		mTimelineScrollWindow.enableHorizontalScroll = true;
		mTimelineScrollWindow.enableVerticalScroll = false;
		mTimelineScrollWindow.enableVerticalResize = false;
		mTimelineScrollWindow.enableMovement = false;
		mTimelineScrollWindow.x = startWidth * 0.2;
		addChild(mTimelineScrollWindow.panel);
		
		mAddBlankFrameButton = new LabeledButton("Add Blank", startWidth * 0.2, startHeight / 4, 0x00FF00, 0x113311);
		mAddBlankFrameButton.addEventListener(MouseEvent.CLICK, addBlankFrameListener);
		addChild(mAddBlankFrameButton);
		mImportButton = new LabeledButton("Import", startWidth * 0.2, startHeight / 4, 0x00FF00, 0x113311);
		mImportButton.addEventListener(MouseEvent.CLICK, importFramesListener);
		mImportButton.y = startHeight / 4;
		addChild(mImportButton);
		mCopyFrameButton = new LabeledButton("Copy", startWidth * 0.2, startHeight / 4, 0x00FF00, 0x113311);
		mCopyFrameButton.addEventListener(MouseEvent.CLICK, copyFramesListener);
		mCopyFrameButton.y = startHeight * 2 / 4;
		addChild(mCopyFrameButton);
		mDeleteFrameButton = new LabeledButton("Delete", startWidth * 0.2, startHeight / 4, 0xFF0000, 0x331111);
		mDeleteFrameButton.addEventListener(MouseEvent.CLICK, deleteFramesListener);
		mDeleteFrameButton.y = startHeight * 3 / 4;
		addChild(mDeleteFrameButton);
		
		mTimelineFrames = [];
		selectedFrames = [];
		setAnimation(null);
	}
	
	private function set_selectedFrames(val:Array<Int>):Array<Int>
	{
		selectedFrames = val.copy();
		if (selectedFrames.length > 0)
		{
			mDeleteFrameButton.color1 = 0xFF0000;
			mDeleteFrameButton.color2 = 0x331111;
			mDeleteFrameButton.mouseEnabled = true;
		}
		else
		{
			mDeleteFrameButton.color1 = 0x888888;
			mDeleteFrameButton.color2 = 0x111111;
			mDeleteFrameButton.mouseEnabled = false;
		}
		highlightSelectedFrames();
		return val;
	}
	
	public function resize(newWidth:Float, newHeight:Float):Void
	{
		mTimelineScrollWindow.width = newWidth * 0.8;
		mTimelineScrollWindow.height = newHeight;
		mTimelineScrollWindow.x = newWidth * 0.2;
		width = newWidth;
		height = newHeight;
		for (f in mTimelineFrames)
			f.setHeight(newHeight - mTimelineScrollWindow.barThickness);
		mAddBlankFrameButton.resize(newWidth * 0.2, newHeight / 4);
		mImportButton.resize(newWidth * 0.2, newHeight / 4);
		mImportButton.y = newHeight / 4;
		mCopyFrameButton.resize(newWidth * 0.2, newHeight / 4);
		mCopyFrameButton.y = newHeight * 2 / 4;
		mDeleteFrameButton.resize(newWidth * 0.2, newHeight / 4);
		mDeleteFrameButton.y = newHeight * 3 / 4;
		arrange();
	}
	
	public function refreshImage():Void
	{
		for (f in mTimelineFrames)
			f.draw();
	}
	public function refreshFramerate():Void
	{
		for (f in mTimelineFrames)
			f.checkDelay();
	}
	
	private function addBlankFrameListener(e:MouseEvent):Void
	{
		mAnimation.addFrame("", 1.0);
		redisplay();
	}
	private function importFramesListener(e:MouseEvent):Void
	{
		var file:File = File.applicationStorageDirectory.resolvePath(BitmapManager.rootDirectory); 
		file.addEventListener(Event.SELECT, completeImportListener);
		file.browseForOpen("Import Image/Animation as Frames", [new FileFilter("Animation/Image File", "*.anx;*.xml;*.png;*.jpg;*.jpeg")]);
	}
	private function completeImportListener(e:Event):Void
	{
		e.target.removeEventListener(Event.SELECT, completeImportListener);
		var root:File = new File().resolvePath(BitmapManager.rootDirectory);
		root.canonicalize();
		var path:String = root.getRelativePath(cast e.target, true);
		if (FileTools.extensionOf(path) == "anx" || FileTools.extensionOf(path) == "xml")
		{
			var a:Animation = Animation.createFromPath(path, false);
			for (i in 0...a.numFrames)
				mAnimation.addFrame(a.getFrame(i).resource, a.getFrame(i).delay, a.getFrame(i).offset, a.getFrame(i).clipRect);
			a.dispose();
			redisplay();
		}
		else
		{
			dispatchEvent(new AnxEditEvent(AnxEditEvent.IMPORT_IMAGE, path));
		}
	}
	@:access(com.problemmachine.animation.Animation.mFrames)
	private function copyFramesListener(e:MouseEvent):Void
	{
		var ordered:Array<Int> = selectedFrames.copy();
		ordered.sort(function(a:Int, b:Int):Int	{	return a - b;		} );
		for (i in ordered)
			mAnimation.mFrames.push(mAnimation.mFrames[i].clone());
		redisplay();
	}
	public function deleteSelectedFrames():Void
	{
		var ordered:Array<Int> = selectedFrames.copy();
		ordered.sort(function(a:Int, b:Int):Int	{	return a - b;		} );
		while (ordered.length > 0)
			mAnimation.removeFrame(ordered.pop());
		redisplay();
	}
	private function deleteFramesListener(e:MouseEvent):Void
	{
		deleteSelectedFrames();
	}
	public inline function redisplay():Void
	{
		var a:Animation = mAnimation;
		mTimelineScrollWindow.setHorizontalScrollPercent(0);
		setAnimation(null);
		setAnimation(a);
	}
	
	public function setAnimation(animation:Animation):Void
	{
		if (animation != mAnimation)
		{
			if (mAnimation != null)
				mAnimation.removeEventListener(AnimationEvent.ANIM_READY, animationReadyListener);
			mAnimation = animation;
			for (f in mTimelineFrames)
			{
				f.removeEventListener(MouseEvent.MOUSE_DOWN, frameListener);
				f.removeEventListener(MouseEvent.RIGHT_MOUSE_DOWN, frameEditImageListener);
				f.removeEventListener(MouseEvent.DOUBLE_CLICK, selectAllFrames);
				f.dispose();
				mTimelineScrollWindow.removeChild(f);
			}
			mTimelineFrames = [];
			selectedFrames = [];
			
			if (animation != null)
			{
				if (animation.ready)
					createFrames();
				else
					animation.addEventListener(AnimationEvent.ANIM_READY, animationReadyListener);
			}
		}
		if (animation != null)
		{
			mAddBlankFrameButton.color1 = mImportButton.color1 = mCopyFrameButton.color1 = 0x00FF00;
			mAddBlankFrameButton.color2 = mImportButton.color2 = mCopyFrameButton.color2 = 0x113311;
			mAddBlankFrameButton.mouseEnabled = mImportButton.mouseEnabled = mCopyFrameButton.mouseEnabled = true;
		}
		else
		{
			mAddBlankFrameButton.color1 = mImportButton.color1 = mCopyFrameButton.color1 = 0x888888;
			mAddBlankFrameButton.color2 = mImportButton.color2 = mCopyFrameButton.color2 = 0x111111;
			mAddBlankFrameButton.mouseEnabled = mImportButton.mouseEnabled = mCopyFrameButton.mouseEnabled = false;
		}
		arrange();
	}
	private function animationReadyListener(e:AnimationEvent):Void
	{
		mAnimation.removeEventListener(AnimationEvent.ANIM_READY, animationReadyListener);
		createFrames();
	}
	private function createFrames():Void
	{
		for (i in 0...mAnimation.numFrames)
			{
				var f:AnxTimelineFrame = new AnxTimelineFrame(mAnimation, i, 100, height - mTimelineScrollWindow.barThickness);
				mTimelineFrames.push(f);
				f.addEventListener(MouseEvent.MOUSE_DOWN, frameListener);
				f.addEventListener(MouseEvent.RIGHT_MOUSE_DOWN, frameEditImageListener);
				f.addEventListener(MouseEvent.DOUBLE_CLICK, selectAllFrames);
			}
		arrange();
	}
		
	private function arrange():Void
	{
		var w:Float = 0;
		for (f in mTimelineFrames)
		{			
			f.x = w;
			w += f.width;
			mTimelineScrollWindow.addChild(f);
		}
	}
	
	private function highlightSelectedFrames():Void
	{
		for (f in mTimelineFrames)
			f.highlight(false, false);
		for (i in selectedFrames)
			mTimelineFrames[i].highlight(true, false);
		if (selectedFrames.length > 0)
			mTimelineFrames[selectedFrames[0]].highlight(true, true);
	}
	
	private function selectAllFrames(e:MouseEvent):Void
	{
		var targetSelect:Int = -1;
		for (i in 0...mTimelineFrames.length)
		{
			if (e.target == mTimelineFrames[i] || e.target.parent == mTimelineFrames[i] || e.target.parent.parent == mTimelineFrames[i])
			{
				targetSelect = i;
				break;
			}
		}
		if (targetSelect == -1)
			throw new Error("this error should never occur");
		selectedFrames = [targetSelect];
		for (i in 0...mAnimation.numFrames)
			if (i != targetSelect)
				selectedFrames.push(i);
		highlightSelectedFrames();
		dispatchEvent(new AnxEditEvent(AnxTimeline.SELECTION_CHANGED));
	}
	
	private function frameListener(e:MouseEvent):Void
	{
		var oldSelected:Array<Int> = selectedFrames.copy();
		var selectOrigin:Int = selectedFrames.length > 0 ? selectedFrames[0] : -1;
		var targetSelect:Int = -1;
		for (i in 0...mTimelineFrames.length)
		{
			if (e.target == mTimelineFrames[i] || e.target.parent == mTimelineFrames[i] || e.target.parent.parent == mTimelineFrames[i])
			{
				targetSelect = i;
				break;
			}
		}
		if (targetSelect == -1)
			throw new Error("this error should never occur");
		
		if (e.shiftKey)
		{
			selectedFrames = [];
			for (i in IntMath.min(selectOrigin, targetSelect)...(IntMath.max(selectOrigin, targetSelect) + 1))
				selectedFrames.push(i);
			if (selectedFrames[0] != selectOrigin)
			{
				selectedFrames.remove(selectOrigin);
				selectedFrames.unshift(selectOrigin);
			}
		}
		else if (e.ctrlKey)
		{
			if (selectedFrames.indexOf(targetSelect) < 0)
				selectedFrames.push(targetSelect);
			else
				selectedFrames.remove(targetSelect);
		}
		else 
		{
			if (selectedFrames.indexOf(targetSelect) < 0)
				selectedFrames = [targetSelect];
			else
			{
				selectedFrames.remove(targetSelect);
				selectedFrames.unshift(targetSelect);
			}
		}
		
		highlightSelectedFrames();
		var changed:Bool = selectedFrames.length != oldSelected.length;
		if (!changed && selectedFrames.length > 0 && oldSelected.length > 0 && (selectedFrames[0] != oldSelected[0]))
			changed = true;
		if (!changed)
			for (i in selectedFrames)
				if (oldSelected.indexOf(i) < 0)
				{
					changed = true;
					break;
				}
		if (changed)
			dispatchEvent(new AnxEditEvent(AnxTimeline.SELECTION_CHANGED));
		if (e.ctrlKey || e.shiftKey || mAnimation.numFrames <= 1 || selectedFrames.indexOf(targetSelect) < 0)
			return;
		if (!Std.is(e.target, AnxTimelineFrame))	// ignore if clicking the frame delay text fields
			return;
		
		var f:AnxTimelineFrame = mTimelineFrames[selectedFrames[0]];
		f.startDrag(false, new Rectangle(0, 0, mTimelineFrames.length * f.width, 0));

		stage.addEventListener(MouseEvent.MOUSE_MOVE, updateDragFrameDisplay);
		stage.addEventListener(MouseEvent.MOUSE_UP, releaseDrag);
		updateDragFrameDisplay();
	}
	private function frameEditImageListener(e:MouseEvent):Void
	{
		var targetSelect:Int = -1;
		for (i in 0...mTimelineFrames.length)
			if (e.target == mTimelineFrames[i] || e.target.parent == mTimelineFrames[i] || e.target.parent.parent == mTimelineFrames[i])
			{
				targetSelect = i;
				break;
			}
		if (selectedFrames.length != 1 || selectedFrames[0] != targetSelect)
		{
			selectedFrames = [targetSelect];
			dispatchEvent(new Event(SELECTION_CHANGED));
		}
		dispatchEvent(new Event(EDIT_FRAME_IMAGE));
	}
	
	private function updateDragFrameDisplay(?e:MouseEvent):Void
	{
		var ordered:Array<Int> = selectedFrames.copy();
		ordered.sort(function(a:Int, b:Int):Int	{	return a - b;		} );
		
		var f:AnxTimelineFrame = mTimelineFrames[selectedFrames[0]];
		var from:Int = selectedFrames[0];
		var to:Int = IntMath.max(determineSelectedFrameOffset(), 
			IntMath.min(Math.floor(f.x / f.width), mTimelineFrames.length - selectedFrames.length + 1));
		
		if (to == from)
		{
			var w:Float = 0;
			for (i in 0...mTimelineFrames.length)
			{
				if (i != selectedFrames[0])
					mTimelineFrames[i].x = w;
				w += mTimelineFrames[i].width;
			}
		}
		else
		{
			var newOrder:Array<Int> = determineNewFrameOrder(from, to);
			
			var w:Float = 0;
			for (i in newOrder)
			{				
				if (i != selectedFrames[0])
					mTimelineFrames[i].x = w;
				w += f.width;
			}
		}
	}
	
	@:access(com.problemmachine.animation.Animation.mFrames)
	private function releaseDrag(e:MouseEvent):Void
	{
		stage.removeEventListener(MouseEvent.MOUSE_MOVE, updateDragFrameDisplay);
		stage.removeEventListener(MouseEvent.MOUSE_UP, releaseDrag);
		mTimelineFrames[selectedFrames[0]].stopDrag();
			
		var f:AnxTimelineFrame = mTimelineFrames[selectedFrames[0]];
		var from:Int = selectedFrames[0];
		var to:Int = IntMath.max(determineSelectedFrameOffset(), 
			IntMath.min(Math.floor(f.x / f.width), mTimelineFrames.length - selectedFrames.length + 1));
			
		if (to == from)
		{
			arrange();
			var changed:Bool = selectedFrames.length != 1;
			selectedFrames = [selectedFrames[0]];
			if (changed)
				dispatchEvent(new AnxEditEvent(AnxTimeline.SELECTION_CHANGED));
			highlightSelectedFrames();
			return;
		}
		
		dispatchEvent(new AnxEditEvent(AnxEditEvent.SAVE_UNDO_STATE));
		
		var newOrder:Array<Int> = determineNewFrameOrder(from, to);
		var frames:Array<Frame> = [];
		var frameTabs:Array<AnxTimelineFrame> = [];
		for (i in newOrder)
		{
			frames.push(mAnimation.mFrames[i]);
			frameTabs.push(mTimelineFrames[i]);
		}
		mAnimation.mFrames = frames;
		mTimelineFrames = frameTabs;
		
		arrange();
	}
	
	private function determineNewFrameOrder(from:Int, to:Int):Array<Int>
	{
		var ordered:Array<Int> = selectedFrames.copy();
			ordered.sort(function(a:Int, b:Int):Int	{	return a - b;		} );
		
		var newOrder:Array<Int> = [];
		for (i in 0...mTimelineFrames.length)
			if (selectedFrames.indexOf(i) < 0)
				newOrder.push(i);
		
		for (i in 0...ordered.length)
			newOrder.insert(to - ordered.indexOf(selectedFrames[0]), ordered[ordered.length - 1 - i]);
		return newOrder;
	}
	
	private function determineSelectedFrameOffset():Int
	{
		var offset:Int = 0;
		for (i in selectedFrames)
			if (i < selectedFrames[0])
				++offset;
		return offset;
	}
	
}