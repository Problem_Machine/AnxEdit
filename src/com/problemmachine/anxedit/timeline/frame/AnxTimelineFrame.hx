package com.problemmachine.anxedit.timeline.frame;
import com.problemmachine.animation.Animation;
import com.problemmachine.animation.Frame;
import com.problemmachine.animation.event.AnimationEvent;
import com.problemmachine.tools.math.IntMath;
import com.problemmachine.tools.text.TextTools;
import com.problemmachine.ui.text.NumberInput;
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.FocusEvent;
import flash.events.MouseEvent;
import flash.geom.Point;
import flash.text.TextField;
import flash.text.TextFieldType;
import flash.text.TextFormat;
import flash.text.TextFormatAlign;

class AnxTimelineFrame extends Sprite
{
	private static inline var DELAY_DISPLAY_HEIGHT:Float = 20;
	
	private var mAnimation:Animation;
	private var mSelected:Bool;
	private var mPrimarySelection:Bool;
	private var mSelectionOverlay:Sprite;
	private var mFrameIndex:Int;
	private var mFrameDelayInput:NumberInput;
	private var mFrameDisplay:Bitmap;
	private var mWidth:Float;
	private var mHeight:Float;

	public function new(animation:Animation, index:Int, width:Float, height:Float) 
	{
		super();
		mWidth = width;
		mHeight = height;
		mAnimation = animation;
		mFrameIndex = index;
		var frame:Frame = mAnimation.getFrame(index);
		mFrameDisplay = new Bitmap();
		draw();
		mFrameDisplay.y = DELAY_DISPLAY_HEIGHT;
		addChild(mFrameDisplay);
		doubleClickEnabled = true;
		
		mFrameDelayInput = new NumberInput(frame.delay, mWidth, DELAY_DISPLAY_HEIGHT, 
			new TextFormat(null, 16, 0x000000, null, null, null, null, null, TextFormatAlign.CENTER), 4, true, true,
			0xFFFFFF, 0xBBBBBB, true);
		mFrameDelayInput.addEventListener(NumberInput.VALUE_CHANGE, frameDelayListener);
		addChild(mFrameDelayInput);
			
		mSelectionOverlay = new Sprite();
		addChild(mSelectionOverlay);
	}
	
	public function draw():Void
	{
		var frame:Frame = mAnimation.getFrame(mFrameIndex);
		if (mFrameDisplay.bitmapData != null)
			mFrameDisplay.bitmapData.dispose();
		if (frame.resource != "" && frame.ready)
		{
			completeDrawingFrame();
		}
		else
		{
			if (frame.resource != "")
				frame.addEventListener(AnimationEvent.ANIM_READY, completeDrawingFrame);
		}
	}
	
	@:access(com.problemmachine.animation.Frame.mAsset)
	private function completeDrawingFrame(?e:Event):Void
	{
		var frame:Frame = mAnimation.getFrame(mFrameIndex);
		frame.removeEventListener(AnimationEvent.ANIM_READY, completeDrawingFrame);
		
		mFrameDisplay.bitmapData = new BitmapData(Math.ceil(frame.clipRect.width), Math.ceil(frame.clipRect.height), true, 0x00000000);
			mFrameDisplay.bitmapData.copyPixels(frame.mAsset, frame.clipRect, new Point());
		var r1:Float = frame.width / frame.height;
		var r2:Float = mWidth / (mHeight - DELAY_DISPLAY_HEIGHT);
		if (r1 > r2)
		{
			mFrameDisplay.width = mWidth;
			mFrameDisplay.height = mWidth / r1;
		}
		else
		{
			mFrameDisplay.width = (mHeight - DELAY_DISPLAY_HEIGHT) * r1;
			mFrameDisplay.height = mHeight - DELAY_DISPLAY_HEIGHT;
		}
	}
	
	public function checkDelay():Void
	{
		var frame:Frame = mAnimation.getFrame(mFrameIndex);
		mFrameDelayInput.value = frame.delay;
	}
	
	public function highlight(on:Bool, primary:Bool):Void
	{
		mSelected = on;
		mPrimarySelection = primary;
		drawSelectionOverlay();
	}
	
	public function setHeight(height:Float):Void
	{
		mFrameDisplay.height = height - DELAY_DISPLAY_HEIGHT;
		drawSelectionOverlay();
	}
	
	private function drawSelectionOverlay():Void
	{
		mSelectionOverlay.graphics.clear();
		if (mSelected)
		{
			mSelectionOverlay.graphics.lineStyle(5, 0xFFFF00);
			mSelectionOverlay.graphics.drawRect(2.5, 2.5, width - 5, height - 5);
			if (mPrimarySelection)
			{
				mSelectionOverlay.graphics.lineStyle(1, 0xFF0000);
				mSelectionOverlay.graphics.drawRect(2.5, 2.5, width - 5, height - 5);
			}
		}
	}
		
	private function frameDelayListener(e:Event):Void
	{
		mAnimation.getFrame(mFrameIndex).delay = mFrameDelayInput.value;
	}
	
	public function dispose():Void
	{
		mAnimation = null;
		if (mFrameDisplay.bitmapData != null)
			mFrameDisplay.bitmapData.dispose();
		mFrameDisplay = null;
		mFrameDelayInput.removeEventListener(NumberInput.VALUE_CHANGE, frameDelayListener);
	}
	
}