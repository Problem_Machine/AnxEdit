package com.problemmachine.anxedit.image;
import com.problemmachine.animation.Animation;
import com.problemmachine.animation.Frame;
import com.problemmachine.bitmap.bitmapmanager.BitmapManager;
import com.problemmachine.tools.file.FileTools;
import com.problemmachine.tools.text.TextTools;
import com.problemmachine.tools.textformat.TextFormatTools;
import com.problemmachine.ui.LabeledButton;
import com.problemmachine.ui.text.NumberInput;
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.filesystem.File;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.text.TextField;
import flash.text.TextFormat;
import net.kaourantin.PNGEnc;

class AnxImageRecompiler extends Sprite
{
	private var mWidth:Float;
	private var mHeight:Float;
	
	private var mIndividualFramesButton:LabeledButton;
	private var mFrameSheetButton:LabeledButton;
	private var mExportAsSheet:Bool;
	
	private var mSaveRowsLabel:TextField;
	private var mSaveRowsInput:NumberInput;
	private var mSaveRowsArrows:Array<Sprite>;
	
	private var mPreviewImage:Bitmap;
	private var mConfirmButton:LabeledButton;
	private var mCancelButton:LabeledButton;
	
	private var mAnimation:Animation;
	private var mCurrentPath:String;
	
	public function new(startWidth:Float, startHeight:Float) 
	{
		super();
		
		mAnimation = null;
		mCurrentPath = null;
		
		mWidth = startWidth;
		mHeight = startHeight;
		
		mSaveRowsLabel = new TextField();
		mSaveRowsLabel.defaultTextFormat = new TextFormat(null, 16, 0xAAAAAA, true);
		mSaveRowsLabel.mouseEnabled = false;
		mSaveRowsLabel.text = "Rows: ";
		addChild(mSaveRowsLabel);
		mSaveRowsInput = new NumberInput(1, 100, 40, new TextFormat(null, 16, 0xFFFFFF), 0, true, true, 0xFFFFFF, 0x111111);
		mSaveRowsInput.addEventListener(NumberInput.VALUE_CHANGE, rowsListener);
		addChild(mSaveRowsInput);
		mSaveRowsArrows = [new Sprite(), new Sprite()];
		mSaveRowsArrows[0].buttonMode = mSaveRowsArrows[0].useHandCursor = mSaveRowsArrows[1].buttonMode = mSaveRowsArrows[1].useHandCursor = true;
		mSaveRowsArrows[0].addEventListener(MouseEvent.CLICK, rowsListener);
		mSaveRowsArrows[1].addEventListener(MouseEvent.CLICK, rowsListener);
		drawArrows(mSaveRowsArrows, startWidth * 0.02, startHeight * 0.05);
		addChild(mSaveRowsArrows[0]);
		addChild(mSaveRowsArrows[1]);
		
		mCancelButton = new LabeledButton("Cancel", 100, 40, 0xFF0000, 0x331111, true);
		mCancelButton.addEventListener(MouseEvent.CLICK, function(e:Event):Void
			{	dispatchEvent(new Event(Event.CLOSE));		} );
		addChild(mCancelButton);
		mConfirmButton = new LabeledButton("Confirm", 100, 40, 0x00FF00, 0x113311, true);
		mConfirmButton.addEventListener(MouseEvent.CLICK, completeRecompile);
		addChild(mConfirmButton);
		
		mExportAsSheet = true;
		mIndividualFramesButton = new LabeledButton("Single", 100, 40, 0x888888, 0x111111, true);
		mIndividualFramesButton.addEventListener(MouseEvent.CLICK, toggleSheetExport);
		addChild(mIndividualFramesButton);
		mFrameSheetButton = new LabeledButton("Sheet", 100, 40, 0xFFFFFF, 0x555555, true);
		mFrameSheetButton.addEventListener(MouseEvent.CLICK, toggleSheetExport);
		addChild(mFrameSheetButton);
		
		mPreviewImage = new Bitmap();
		addChild(mPreviewImage);
		
		resize(startWidth, startHeight);
	}
	
	public function resize(newWidth:Float, newHeight:Float):Void
	{
		mWidth = newWidth;
		mHeight = newHeight;
		
		graphics.clear();
		graphics.lineStyle(5, 0xAAAAAA);
		graphics.beginFill(0x333333);
		graphics.drawRect(2.5, 2.5, newWidth - 5, newHeight - 5);
		
		mSaveRowsLabel.width = newWidth * 0.1;
		mSaveRowsLabel.x = 0;
		mSaveRowsLabel.height = newHeight * 0.05;
		mSaveRowsInput.resize(newWidth * 0.05, newHeight * 0.05);
		TextTools.scaleTextToFit(mSaveRowsLabel);
		drawArrows(mSaveRowsArrows, newWidth * 0.02, newHeight * 0.05);
		mSaveRowsArrows[0].x = mSaveRowsLabel.x + mSaveRowsLabel.width;
		mSaveRowsInput.x = mSaveRowsArrows[0].x + mSaveRowsArrows[0].width;
		mSaveRowsArrows[1].x = mSaveRowsInput.x + mSaveRowsInput.width;
		mSaveRowsLabel.y =  mSaveRowsInput.y = mSaveRowsArrows[0].y = mSaveRowsArrows[1].y = newHeight * 0.9;
		
		mFrameSheetButton.resize(newWidth * 0.2, newHeight * 0.05);
		mFrameSheetButton.x = newWidth * 0.2;
		mFrameSheetButton.y = newHeight * 0.9;
		mIndividualFramesButton.resize(newWidth * 0.2, newHeight * 0.05);
		mIndividualFramesButton.x = newWidth * 0.2;
		mIndividualFramesButton.y = newHeight * 0.95;
		
		mCancelButton.resize(newWidth * 0.3, newHeight * 0.1);
		mCancelButton.x = newWidth * 0.4;
		mCancelButton.y = newHeight * 0.9;
		mConfirmButton.resize(newWidth * 0.3, newHeight * 0.1);
		mConfirmButton.x = newWidth * 0.7;
		mConfirmButton.y = newHeight * 0.9;
		
		if (mPreviewImage.bitmapData != null)
			scaleImagePreview();
	}
	
	public function beginRecompile(animation:Animation, animationPath:String):Void
	{
		mAnimation = animation;
		mCurrentPath = animationPath;
		mExportAsSheet = true;
		mSaveRowsInput.value = 1;
		updateImagePreview();
	}
	
	public function endRecompile():Void
	{
		mAnimation = null;
	}
	
	public function getUpdatedAnimation():Animation
	{
		return mAnimation;
	}
	
	@:access(com.problemmachine.animation.Frame.mAsset)
	private function updateImagePreview():Void
	{			
		//TODO: display for separate image export
		var maxWidth:Float = 0;
		var maxHeight:Float = 0;
		var maxWidthThisRow:Float = 0;
		var maxHeightThisRow:Float = 0;
		for (i in 0...mAnimation.numFrames)
		{
			var f:Frame = mAnimation.getFrame(i);
			
			maxWidthThisRow += f.clipRect.width;
			maxHeightThisRow = Math.max(maxHeightThisRow, f.clipRect.height);
			
			if ((i + 1) % Math.ceil(mAnimation.numFrames / mSaveRowsInput.value) == 0)
			{
				maxWidth = Math.max(maxWidth, maxWidthThisRow);
				maxHeight += maxHeightThisRow;
				maxHeightThisRow = maxWidthThisRow = 0;
			}
		}
		if (mAnimation.numFrames % Math.ceil(mAnimation.numFrames / mSaveRowsInput.value) != 0)
			maxHeight += maxHeightThisRow;
		if (mPreviewImage.bitmapData != null)
		{
			if (mPreviewImage.bitmapData.width == maxWidth && mPreviewImage.bitmapData.height == maxHeight)
				mPreviewImage.bitmapData.fillRect(new Rectangle(0, 0, maxWidth, maxHeight), 0x00000000);
			else
			{
				mPreviewImage.bitmapData.dispose();
				mPreviewImage.bitmapData = new BitmapData(Math.ceil(maxWidth), Math.ceil(maxHeight), true, 0x00000000);
			}
		}
		else
			mPreviewImage.bitmapData = new BitmapData(Math.ceil(maxWidth), Math.ceil(maxHeight), true, 0x00000000);
		var bmp:BitmapData = mPreviewImage.bitmapData;
		
		var p:Point = new Point();
		maxHeightThisRow = 0;
		for (i in 0...mAnimation.numFrames)
		{
			var f:Frame = mAnimation.getFrame(i);
			bmp.copyPixels(f.mAsset, f.clipRect, p);
			
			p.x += f.clipRect.width;
			maxHeightThisRow = Math.max(maxHeightThisRow, f.clipRect.height);
			if ((i + 1) % Math.ceil(mAnimation.numFrames / mSaveRowsInput.value) == 0)
			{
				p.y += maxHeightThisRow;
				maxHeightThisRow = p.x = 0;
			}
		}
		scaleImagePreview();
	}
	private function scaleImagePreview():Void
	{
		var r1:Float = mPreviewImage.bitmapData.width / mPreviewImage.bitmapData.height;
		var r2:Float = mWidth / (mHeight * 0.9);
		if (r1 > r2)
		{
			mPreviewImage.width = mWidth;
			mPreviewImage.height = mWidth / r1;
		}
		else
		{
			mPreviewImage.width = (mHeight * 0.9) * r1;
			mPreviewImage.height = (mHeight * 0.9);
		}
	}
	
	private function completeRecompile(e:MouseEvent):Void
	{
		var file:File = new File(BitmapManager.rootDirectory);
		file.canonicalize();
		file = file.resolvePath(FileTools.filenameOf(mCurrentPath, true)); 
		file.addEventListener(Event.SELECT, selectAnimationPathListener);
		file.addEventListener(Event.CANCEL, cancelRecompile);
		file.browseForSave("Save Recompiled Animation");
	}
	private function selectAnimationPathListener(e:Event):Void
	{
		mCurrentPath = cast(e.target, File).nativePath;
		cast(e.target, File).removeEventListener(Event.SELECT, selectAnimationPathListener);
		cast(e.target, File).removeEventListener(Event.CANCEL, cancelRecompile);
		var file:File = File.applicationStorageDirectory.resolvePath(BitmapManager.rootDirectory + mAnimation.getFrame(0).resource); 
		file.addEventListener(Event.SELECT, selectImagePathListener);
		file.addEventListener(Event.CANCEL, cancelRecompile);
		file.browseForSave("Save Recompiled Image(s)");
	}
	private function selectImagePathListener(e:Event):Void
	{
		var imagePath:String = cast(e.target, File).nativePath;
		cast(e.target, File).removeEventListener(Event.SELECT, selectImagePathListener);
		cast(e.target, File).removeEventListener(Event.CANCEL, cancelRecompile);
		mAnimation = saveData(imagePath, mCurrentPath);
		dispatchEvent(new Event(Event.COMPLETE));
	}
	private function cancelRecompile(e:Event):Void
	{
		cast(e.target, File).removeEventListener(Event.SELECT, selectAnimationPathListener);
		cast(e.target, File).removeEventListener(Event.SELECT, selectImagePathListener);
		cast(e.target, File).removeEventListener(Event.CANCEL, cancelRecompile);
	}
	
	@:access(com.problemmachine.animation.Frame.mAsset)
	private function saveData(imagePath:String, animationPath:String):Animation
	{
		var an:Animation = mAnimation.clone();
		var root:File = new File().resolvePath(BitmapManager.rootDirectory);
		root.canonicalize();
		if (mExportAsSheet)
		{
			var relImagePath:String = root.getRelativePath(new File(imagePath));
			FileTools.immediateWriteTo(imagePath, PNGEnc.encode(mPreviewImage.bitmapData));
			var p:Point = new Point();
			var maxHeightThisRow:Float = 0;
			for (i in 0...mAnimation.numFrames)
			{
				var f:Frame = mAnimation.getFrame(i);
				an.getFrame(i).resource = relImagePath;
				an.getFrame(i).clipRect = new Rectangle(p.x, p.y, f.clipRect.width, f.clipRect.height);
				
				p.x += f.clipRect.width;
				maxHeightThisRow = Math.max(maxHeightThisRow, f.clipRect.height);
				if ((i + 1) % Math.ceil(mAnimation.numFrames / mSaveRowsInput.value) == 0)
				{
					p.y += maxHeightThisRow;
					maxHeightThisRow = p.x = 0;
				}
			}
			if (mAnimation.numFrames % Math.ceil(mAnimation.numFrames / mSaveRowsInput.value) != 0)
				p.y += maxHeightThisRow;
		}
		else
		{
			imagePath = FileTools.formatPath(imagePath);
			imagePath = imagePath.substring(0, imagePath.lastIndexOf(".png"));
			var digits:Int = 1;
			var len:Int = mAnimation.numFrames;
			while ((len = Math.floor(len / 10)) != 0)
				++digits;
			for (i in 0...mAnimation.numFrames)
			{
				var f:Frame = mAnimation.getFrame(i);
				var bmp:BitmapData = new BitmapData(Math.ceil(f.clipRect.width), Math.ceil(f.clipRect.height), true, 0x00000000);
				bmp.copyPixels(f.mAsset, f.clipRect, new Point(0, 0));
				var num:String = Std.string(i);
				while (num.length < digits)
					num = "0" + num;
				FileTools.immediateWriteTo(imagePath + num + ".png", PNGEnc.encode(bmp));
				an.getFrame(i).resource = root.getRelativePath(new File(imagePath + num + ".png"));
				an.getFrame(i).clipRect = null;
			}
		}
		FileTools.immediateWriteTo(mCurrentPath, an.toXML());
		return an;
	}
	
	private function toggleSheetExport(e:MouseEvent):Void
	{
		mExportAsSheet = !mExportAsSheet;
		if (mExportAsSheet)
		{
			mFrameSheetButton.color1 = 0xFFFFFF;
			mFrameSheetButton.color2 = 0x555555;
			mIndividualFramesButton.color1 = 0x888888;
			mIndividualFramesButton.color2 = 0x111111;
		}
		else
		{
			mIndividualFramesButton.color1 = 0xFFFFFF;
			mIndividualFramesButton.color2 = 0x555555;
			mFrameSheetButton.color1 = 0x888888;
			mFrameSheetButton.color2 = 0x111111;
		}
		updateImagePreview();
	}
	
	private function rowsListener(e:MouseEvent):Void
	{
		var oldVal:Int = Std.int(mSaveRowsInput.value);
		if (e.target == mSaveRowsArrows[0])
			mSaveRowsInput.value = Math.max(1, mSaveRowsInput.value - 1);
		else if (e.target == mSaveRowsArrows[1])
			++mSaveRowsInput.value;
		else
			mSaveRowsInput.value = Math.max(1, mSaveRowsInput.value);
		if (oldVal != mSaveRowsInput.value)
			updateImagePreview();
	}
	
	private function drawArrows(arrows:Array<Sprite>, width:Float, height:Float):Void
	{
		// yeah i copied this from AnxImageImporter big whoop wanna fight about it?
		for (s in arrows)
		{
			s.graphics.lineStyle(3, 0xFFFFFF);
			s.graphics.beginFill(0x333333);
			s.graphics.drawRect(0, 0, width, height);
		}
		arrows[0].graphics.beginFill(0x777777);
		arrows[0].graphics.moveTo(width * 0.7, height * 0.2);
		arrows[0].graphics.lineTo(width * 0.2, height * 0.5);
		arrows[0].graphics.lineTo(width * 0.7, height * 0.8);
		arrows[0].graphics.lineTo(width * 0.7, height * 0.2);
		arrows[1].graphics.beginFill(0x777777);
		arrows[1].graphics.moveTo(width * 0.3, height * 0.2);
		arrows[1].graphics.lineTo(width * 0.8, height * 0.5);
		arrows[1].graphics.lineTo(width * 0.3, height * 0.8);
		arrows[1].graphics.lineTo(width * 0.3, height * 0.2);
	}
}