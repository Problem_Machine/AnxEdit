package com.problemmachine.anxedit.image;

import com.problemmachine.animation.Frame;
import com.problemmachine.anxedit.event.AnxEditEvent;
import com.problemmachine.bitmap.bitmapmanager.BitmapManager;
import com.problemmachine.bitmap.bitmapmanager.BitmapManagerEvent;
import com.problemmachine.bitmap.bitmapmanager.BitmapManagerQuery;
import com.problemmachine.tools.file.FileTools;
import com.problemmachine.tools.text.TextTools;
import com.problemmachine.ui.scrollwindow.ScrollWindow;
import com.problemmachine.ui.text.NumberInput;
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Sprite;
import flash.errors.Error;
import flash.events.Event;
import flash.events.FocusEvent;
import flash.events.MouseEvent;
import flash.events.TimerEvent;
import flash.filesystem.File;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.net.FileFilter;
import flash.text.TextField;
import flash.text.TextFieldType;
import flash.text.TextFormat;
import flash.utils.Timer;

class AnxFrameImageEditor extends Sprite
{
	public var path(default, set):String;
	public var rect(default, default):Rectangle;
	
	private var mDisplayWindow:ScrollWindow;
	private var mBackground:Sprite;
	private var mImageDisplay:Bitmap;
	private var mClipDisplay:Sprite;
	
	private var mImagePathContainer:Sprite;
	private var mImagePathDisplay:TextField;
	private var mCloseButton:Sprite;
	
	private var mClipRectangle:Rectangle;
	private var mClipRectInputLabels:Array<TextField>;
	private var mClipRectInputs:Array<NumberInput>;
	
	private var mDraggingSide:String;
	
	private var mTimer:Timer;
	
	public function new(startWidth:Float, startHeight:Float) 
	{
		super();
		
		mBackground = new Sprite();
		mBackground.addEventListener(MouseEvent.MOUSE_DOWN, dragImageListener);
		mBackground.useHandCursor = true;
		
		mClipRectInputLabels = [];
		mClipRectInputs = [];
		for (i in 0...8)
		{
			var tf:TextField = new TextField();
			tf.defaultTextFormat = new TextFormat(null, 16, 0xAAAAAA, true);
			tf.mouseEnabled = false;
			addChild(tf);
			mClipRectInputLabels.push(tf);
			
			var num:NumberInput = new NumberInput(0, 100, 40, new TextFormat(null, 16, 0xFFFFFF), 2, true, true, 0xFFFFFF, 0x111111);
			num.addEventListener(NumberInput.VALUE_CHANGE, rectChangeListener);
			addChild(num);
			num.autoSize = true;
			mClipRectInputs.push(num);
		}
		
		mClipRectInputLabels[0].text = "X: ";			mClipRectInputLabels[1].text = "Y: ";
		mClipRectInputLabels[2].text = "Width: ";		mClipRectInputLabels[3].text = "Height: ";
		mClipRectInputLabels[4].text = "Left: ";		mClipRectInputLabels[5].text = "Top: ";
		mClipRectInputLabels[6].text = "Right: ";		mClipRectInputLabels[7].text = "Bottom: ";
		
		mImagePathContainer = new Sprite();
		mImagePathContainer.useHandCursor = mImagePathContainer.buttonMode = true;
		mImagePathContainer.addEventListener(MouseEvent.CLICK, loadImageListener);
		mImagePathDisplay = new TextField();
		mImagePathDisplay.defaultTextFormat = new TextFormat("Consolas", 16, 0xFFFFFF);
		mImagePathDisplay.background = true;
		mImagePathDisplay.backgroundColor = 0x000000;
		mImagePathDisplay.mouseEnabled = false;
		mImagePathContainer.addChild(mImagePathDisplay);
		mCloseButton = new Sprite();
		mCloseButton.useHandCursor = mCloseButton.buttonMode = true;
		mCloseButton.addEventListener(MouseEvent.CLICK, closeEditorListener);
		
		mDisplayWindow = new ScrollWindow(startWidth * 0.8, startHeight * 0.9, 0xAAAAAA, 0x333333, 30);
		mDisplayWindow.enableHorizontalResize = false;
		mDisplayWindow.enableHorizontalScroll = true;
		mDisplayWindow.enableMovement = false;
		mDisplayWindow.enableVerticalResize = false;
		mDisplayWindow.enableVerticalScroll = true;
		mClipDisplay = new Sprite();
		mClipDisplay.buttonMode = mClipDisplay.useHandCursor = true;
		mClipDisplay.addEventListener(MouseEvent.MOUSE_DOWN, dragRectListener);
		mImageDisplay = new Bitmap();
		
		addChild(mDisplayWindow.panel);
		addChild(mImagePathContainer);
		addChild(mCloseButton);		
		mDisplayWindow.addChild(mBackground);
		mBackground.addChild(mImageDisplay);
		mBackground.addChild(mClipDisplay);
		
		mTimer = new Timer(10);
		mTimer.addEventListener(TimerEvent.TIMER, updateRect);
		addEventListener(Event.ADDED_TO_STAGE, stageListener);
		addEventListener(Event.REMOVED_FROM_STAGE, stageListener);
		
		resize(startWidth, startHeight);
	}
	
	private function set_path(val:String):String
	{
		if (val != path)
		{
			if (path != null)
			{
				BitmapManager.unlock(path, this);
				BitmapManager.stopListeningForCompletion(path, completeImageLoad);
			}
			path = val;
			if (path == null)
			{
				mImageDisplay.bitmapData = null;
				mImagePathDisplay.text = "";
			}
			else
			{
				mImagePathDisplay.text = path;
				BitmapManager.loadAndLock(path, this);
				if (BitmapManager.query(path) != BitmapManagerQuery.Ready)
					BitmapManager.startListeningForCompletion(path, completeImageLoad);
				else
					completeImageLoad();
			}
			TextTools.scaleTextToFit(mImagePathDisplay);
		}
		return val;
	}
	private function completeImageLoad(?e:BitmapManagerEvent):Void
	{
		var bd:BitmapData = BitmapManager.request(path);
		mClipRectangle = new Rectangle(0, 0, bd.width, bd.height);
		mImageDisplay.bitmapData = bd;
		mBackground.graphics.clear();
		mBackground.graphics.beginFill(0xFF00FF);
		mBackground.graphics.drawRect(0, 0, bd.width, bd.height);
	}
	
	public function setFrame(frame:Frame):Void
	{
		path = frame.resource;
		mClipRectangle = frame.clipRect != null ? frame.clipRect.clone() : new Rectangle();
	}
	
	public function finalizeFrame(frame:Frame):Void
	{
		frame.resource = path;
		frame.clipRect = mClipRectangle.clone();
	}
	
	public function resize(newWidth:Float, newHeight:Float):Void
	{
		graphics.clear();
		graphics.lineStyle(5, 0xAAAAAA);
		graphics.beginFill(0x333333);
		graphics.drawRect(2.5, 2.5, newWidth - 5, newHeight - 5);
		
		for (i in 0...mClipRectInputLabels.length)
		{
			mClipRectInputs[i].width = mClipRectInputLabels[i].width = newWidth * 0.1;
			mClipRectInputs[i].height = mClipRectInputLabels[i].height= newHeight / mClipRectInputLabels.length;
			mClipRectInputLabels[i].x = 0;
			mClipRectInputs[i].x = newWidth * 0.1;
			mClipRectInputs[i].y = mClipRectInputLabels[i].y = newHeight * (i / 8);
		}
		
		mImagePathContainer.graphics.clear();
		mImagePathContainer.graphics.lineStyle(3, 0xFFFFFF);
		mImagePathContainer.graphics.drawRect(1.5, 1.5, newWidth * 0.8 - 3, 50 - 3);
		mImagePathContainer.x = newWidth * 0.2;
		mImagePathContainer.y = newHeight - 50;
		mImagePathDisplay.x = 3;
		mImagePathDisplay.y = 3;
		mImagePathDisplay.width = newWidth * 0.8 - 6;
		mImagePathDisplay.height = 50 - 6;
		TextTools.scaleTextToFit(mImagePathDisplay);
		
		mCloseButton.graphics.clear();
		mCloseButton.graphics.lineStyle(5, 0xFF0000);
		mCloseButton.graphics.beginFill(0x330000);
		mCloseButton.graphics.drawRect(2.5, 2.5, newWidth * 0.05 - 5, newHeight * 0.05 - 5);
		mCloseButton.graphics.moveTo(newWidth * 0.01, newHeight * 0.01);	mCloseButton.graphics.lineTo(newWidth * 0.04, newHeight * 0.04);
		mCloseButton.graphics.moveTo(newWidth * 0.04, newHeight * 0.01);	mCloseButton.graphics.lineTo(newWidth * 0.01, newHeight * 0.04);
		mCloseButton.x = newWidth * 0.95;
		
		mDisplayWindow.width = newWidth * 0.8;
		mDisplayWindow.height = newHeight * 0.9;
		mDisplayWindow.x = newWidth * 0.2;
	}
	
	private function stageListener(e:Event):Void
	{
		if (e.type == Event.ADDED_TO_STAGE)
			mTimer.start();
		else
			mTimer.stop();
	}
	private function dragImageListener(e:MouseEvent):Void
	{
		if (e.type == MouseEvent.MOUSE_DOWN)
		{
			if (e.target == mClipDisplay)
				return;
			mBackground.startDrag();
			stage.addEventListener(MouseEvent.MOUSE_UP, dragImageListener);
		}
		else
		{
			mBackground.stopDrag();
			stage.removeEventListener(MouseEvent.MOUSE_UP, dragImageListener);
			if (mBackground.x < -mImageDisplay.bitmapData.width + 20)
				mBackground.x = -mImageDisplay.bitmapData.width + 20;
			else if (mBackground.x > mDisplayWindow.width - mDisplayWindow.barThickness - 20)
				mBackground.x = mDisplayWindow.width - mDisplayWindow.barThickness - 20;
			if (mBackground.y < -mImageDisplay.bitmapData.height + 20)
				mBackground.y = -mImageDisplay.bitmapData.height + 20;
			else if (mBackground.y > mDisplayWindow.height - mDisplayWindow.barThickness)
				mBackground.y = mDisplayWindow.height - mDisplayWindow.barThickness - 20;
		}
	}
	private function loadImageListener(e:MouseEvent):Void
	{
		var file:File = File.applicationStorageDirectory.resolvePath(BitmapManager.rootDirectory + path); 
		file.addEventListener(Event.SELECT, completeLoadImageListener);
		file.browseForOpen("Load Image File", [new FileFilter("Image File", "*.png;*.jpg;*.jpeg;*.gif")]);
	}
	private function completeLoadImageListener(e:Event):Void
	{
		var root:File = new File().resolvePath(BitmapManager.rootDirectory);
		root.canonicalize();
		if (!FileTools.comparePaths(root.getRelativePath(cast e.target, true), path))
		{
			dispatchEvent(new AnxEditEvent(AnxEditEvent.SAVE_UNDO_STATE));
			path = root.getRelativePath(cast e.target, true);
		}
	}
	private function closeEditorListener(e:MouseEvent):Void
	{
		dispatchEvent(new Event(Event.CLOSE));
	}
	
	private function updateRect(e:TimerEvent):Void
	{
		if (!visible)
			return;
		mClipDisplay.graphics.clear();
		mClipDisplay.graphics.beginFill(0x000000);
		mClipDisplay.graphics.drawRect(mClipRectangle.x - 10, mClipRectangle.y - 10, mClipRectangle.width + 20, mClipRectangle.height + 20);
		mClipDisplay.graphics.drawRect(mClipRectangle.x, mClipRectangle.y, mClipRectangle.width, mClipRectangle.height);
		if (!currentlyEditingText())
		{
			mClipRectInputs[0].value = mClipRectangle.x;
			mClipRectInputs[1].value = mClipRectangle.y;
			mClipRectInputs[2].value = mClipRectangle.width;
			mClipRectInputs[3].value = mClipRectangle.height;
			mClipRectInputs[4].value = mClipRectangle.left;
			mClipRectInputs[5].value = mClipRectangle.top;
			mClipRectInputs[6].value = mClipRectangle.right;
			mClipRectInputs[7].value = mClipRectangle.bottom;
		}
	}
	
	private function rectChangeListener(e:Event):Void
	{
		dispatchEvent(new AnxEditEvent(AnxEditEvent.SAVE_UNDO_STATE));
		var num:NumberInput = cast e.target;
		if (num == mClipRectInputs[0])
			mClipRectangle.x = num.value;
		else if (num == mClipRectInputs[1])
			mClipRectangle.y = num.value;
		else if (num == mClipRectInputs[2])
			mClipRectangle.width = num.value;
		else if (num == mClipRectInputs[3])
			mClipRectangle.height = num.value;
		else if (num == mClipRectInputs[4])
			mClipRectangle.left = num.value;
		else if (num == mClipRectInputs[5])
			mClipRectangle.top = num.value;
		else if (num == mClipRectInputs[6])
			mClipRectangle.right = num.value;
		else if (num == mClipRectInputs[7])
			mClipRectangle.bottom = num.value;	
		else throw new Error("problem");
	}
	
	private function currentlyEditingText():Bool
	{
		for (num in mClipRectInputs)
			if (num.editing)
				return true;
		return false;
	}
	
	private function dragRectListener(e:MouseEvent):Void
	{
		if (mImageDisplay.mouseX <= mClipRectangle.left)
		{
			if (mImageDisplay.mouseY <= mClipRectangle.top)
				mDraggingSide = "TopLeft";
			else if (mImageDisplay.mouseY >= mClipRectangle.bottom)
				mDraggingSide = "BottomLeft";
			else
				mDraggingSide = "Left";
		}
		else if (mImageDisplay.mouseX >= mClipRectangle.right)
		{
			if (mImageDisplay.mouseY <= mClipRectangle.top)
				mDraggingSide = "TopRight";
			else if (mImageDisplay.mouseY >= mClipRectangle.bottom)
				mDraggingSide = "BottomRight";
			else
				mDraggingSide = "Right";
		}
		else
		{
			if (mImageDisplay.mouseY <= mClipRectangle.top)
				mDraggingSide = "Top";
			else if (mImageDisplay.mouseY >= mClipRectangle.bottom)
				mDraggingSide = "Bottom";
			else
				throw new Error("AnxFrameImageEditor.dragRectListener() - This mouse location should be impossible");
		}
		dispatchEvent(new AnxEditEvent(AnxEditEvent.SAVE_UNDO_STATE));
		stage.addEventListener(MouseEvent.MOUSE_MOVE, updateDragRectListener);
		stage.addEventListener(MouseEvent.MOUSE_UP, completeDragRectListener);
	}
	
	private function updateDragRectListener(e:MouseEvent):Void
	{
		switch(mDraggingSide)
		{
			case "TopLeft":
				mClipRectangle.left = mImageDisplay.mouseX;
				mClipRectangle.top = mImageDisplay.mouseY;
			case "Top":
				mClipRectangle.top = mImageDisplay.mouseY;
			case "TopRight":
				mClipRectangle.right = mImageDisplay.mouseX;
				mClipRectangle.top = mImageDisplay.mouseY;
			case "Right":
				mClipRectangle.right = mImageDisplay.mouseX;
			case "BottomRight":
				mClipRectangle.right = mImageDisplay.mouseX;
				mClipRectangle.bottom = mImageDisplay.mouseY;
			case "Bottom":
				mClipRectangle.bottom = mImageDisplay.mouseY;
			case "BottomLeft":
				mClipRectangle.left = mImageDisplay.mouseX;
				mClipRectangle.bottom = mImageDisplay.mouseY;
			case "Left":
				mClipRectangle.left = mImageDisplay.mouseX;
			default:
				throw new Error("oops");
		}
		if (mClipRectangle.left < 0)
			mClipRectangle.left = 0;
		if (mClipRectangle.right > mImageDisplay.bitmapData.width)
			mClipRectangle.right = mImageDisplay.bitmapData.width;
		if (mClipRectangle.top < 0)
			mClipRectangle.top = 0;
		if (mClipRectangle.bottom > mImageDisplay.bitmapData.height)
			mClipRectangle.bottom = mImageDisplay.bitmapData.height;
	}
	
	private function completeDragRectListener(e:MouseEvent):Void
	{
		mDraggingSide = null;
		stage.removeEventListener(MouseEvent.MOUSE_MOVE, updateDragRectListener);
		stage.removeEventListener(MouseEvent.MOUSE_UP, completeDragRectListener);
	}
}