package com.problemmachine.anxedit.image;
import com.problemmachine.animation.Frame;
import com.problemmachine.bitmap.bitmapmanager.BitmapManager;
import com.problemmachine.bitmap.bitmapmanager.BitmapManagerEvent;
import com.problemmachine.tools.text.TextTools;
import com.problemmachine.ui.LabeledButton;
import com.problemmachine.ui.text.NumberInput;
import flash.display.Bitmap;
import flash.display.Graphics;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.filesystem.File;
import flash.geom.Rectangle;
import flash.net.FileFilter;
import flash.text.TextField;
import flash.text.TextFormat;
import flash.text.TextFormatAlign;

class AnxImageImporter extends Sprite
{
	private var mWidth:Float;
	private var mHeight:Float;
	
	private var mColumnsLabel:TextField;
	private var mColumnsInput:NumberInput;
	private var mColumnsArrows:Array<Sprite>;
	private var mRowsLabel:TextField;
	private var mRowsInput:NumberInput;
	private var mRowsArrows:Array<Sprite>;
	private var mDelayLabel:TextField;
	private var mDelayInput:NumberInput;
	
	private var mRowColumnOrderButton:Sprite;
	private var mRowsFirst:Bool;
	private var mFrameNumbers:Array<TextField>;
	private var mWarningSign:TextField;	// for warning of unequal division of frames
	
	private var mImageDisplay:Bitmap;
	private var mImageDisplayOverlay:Sprite;
	
	private var mImagePathContainer:Sprite;
	private var mImagePathDisplay:TextField;
	private var mCloseButton:Sprite;
	private var mConfirmButton:LabeledButton;

	public function new(startWidth:Float, startHeight:Float) 
	{
		super();
		
		mWidth = startWidth;
		mHeight = startHeight;
		
		mColumnsLabel = new TextField();
		mColumnsLabel.defaultTextFormat = new TextFormat(null, 16, 0xAAAAAA, true);
		mColumnsLabel.mouseEnabled = false;
		mColumnsLabel.text = "Columns: ";
		addChild(mColumnsLabel);
		mColumnsInput = new NumberInput(1, 100, 40, new TextFormat(null, 16, 0xFFFFFF), 0, true, true, 0xFFFFFF, 0x111111);
		mColumnsInput.addEventListener(NumberInput.VALUE_CHANGE, valueChangeListener);
		addChild(mColumnsInput);
		mColumnsArrows = [new Sprite(), new Sprite()];
		mColumnsArrows[0].buttonMode = mColumnsArrows[0].useHandCursor = mColumnsArrows[1].buttonMode = mColumnsArrows[1].useHandCursor = true;
		mColumnsArrows[0].addEventListener(MouseEvent.CLICK, arrowListener);
		mColumnsArrows[1].addEventListener(MouseEvent.CLICK, arrowListener);
		drawArrows(mColumnsArrows, startWidth * 0.02, startHeight * 0.05);
		addChild(mColumnsArrows[0]);
		addChild(mColumnsArrows[1]);
		
		mRowsLabel = new TextField();
		mRowsLabel.defaultTextFormat = new TextFormat(null, 16, 0xAAAAAA, true);
		mRowsLabel.mouseEnabled = false;
		mRowsLabel.text = "Rows: ";
		addChild(mRowsLabel);
		mRowsInput = new NumberInput(1, 100, 40, new TextFormat(null, 16, 0xFFFFFF), 0, true, true, 0xFFFFFF, 0x111111);
		mRowsInput.addEventListener(NumberInput.VALUE_CHANGE, valueChangeListener);
		addChild(mRowsInput);
		mRowsArrows = [new Sprite(), new Sprite()];
		mRowsArrows[0].buttonMode = mRowsArrows[0].useHandCursor = mRowsArrows[1].buttonMode = mRowsArrows[1].useHandCursor = true;
		mRowsArrows[0].addEventListener(MouseEvent.CLICK, arrowListener);
		mRowsArrows[1].addEventListener(MouseEvent.CLICK, arrowListener);
		drawArrows(mRowsArrows, startWidth * 0.02, startHeight * 0.05);
		addChild(mRowsArrows[0]);
		addChild(mRowsArrows[1]);
		
		mDelayLabel = new TextField();
		mDelayLabel.defaultTextFormat = new TextFormat(null, 16, 0xAAAAAA, true);
		mDelayLabel.mouseEnabled = false;
		mDelayLabel.text = "Delay: ";
		addChild(mDelayLabel);
		mDelayInput = new NumberInput(0.0167, 100, 40, new TextFormat(null, 16, 0xFFFFFF), 3, true, true, 0xFFFFFF, 0x111111);
		addChild(mDelayInput);
		mConfirmButton = new LabeledButton("Confirm", 100, 40, 0xFFFFFF, 0x333333, true);
		mConfirmButton.addEventListener(MouseEvent.CLICK, confirmButtonListener);
		addChild(mConfirmButton);
		
		mImagePathContainer = new Sprite();
		mImagePathContainer.useHandCursor = mImagePathContainer.buttonMode = true;
		mImagePathContainer.addEventListener(MouseEvent.CLICK, loadImageListener);
		mImagePathDisplay = new TextField();
		mImagePathDisplay.defaultTextFormat = new TextFormat("Consolas", 16, 0xFFFFFF);
		mImagePathDisplay.background = true;
		mImagePathDisplay.backgroundColor = 0x000000;
		mImagePathDisplay.mouseEnabled = false;
		mImagePathContainer.addChild(mImagePathDisplay);
		mCloseButton = new Sprite();
		mCloseButton.useHandCursor = mCloseButton.buttonMode = true;
		mCloseButton.addEventListener(MouseEvent.CLICK, closeButtonListener);
		
		mImageDisplay = new Bitmap();
		mImageDisplayOverlay = new Sprite();
		
		addChild(mImagePathContainer);
		addChild(mImageDisplay);
		addChild(mImageDisplayOverlay);
		addChild(mCloseButton);
		
		mFrameNumbers = [];
		
		// TODO: Rows/Columns first button, warn of unequal division
		
		resize(startWidth, startHeight);
	}
	
	public function getFrames():Array<Frame>
	{
		var path:String = mImagePathDisplay.text;
		if (path == "")
			return [];
		var columns:Int = cast mColumnsInput.value;
		var rows:Int = cast mRowsInput.value;
		var w:Int = Math.floor(mImageDisplay.bitmapData.width / columns);
		var h:Int = Math.floor(mImageDisplay.bitmapData.height / rows);
		var frames:Array<Frame> = [];
		if (mRowsFirst)
		{
			for (i in 0...columns)
				for (j in 0...rows)
					frames.push(new Frame(path, mDelayInput.value, null, new Rectangle(i * w, j * h, w, h)));
		}
		else
		{
			for (i in 0...rows)
				for (j in 0...columns)
					frames.push(new Frame(path, mDelayInput.value, null, new Rectangle(j * w, i * h, w, h)));
		}
		return frames;
	}
	
	public function setImage(?path:String):Void
	{
		if (path == null)
			path = "";
		if (mImagePathDisplay.text != path && mImagePathDisplay.text != "")
		{
			BitmapManager.unlock(mImagePathDisplay.text, this);
			BitmapManager.stopListeningForCompletion(path, fileListener);
		}
		mImagePathDisplay.text = path;
		if (path != "")
		{
			TextTools.scaleTextToFit(mImagePathDisplay);
			
			if (BitmapManager.query(path) == Ready)
			{
				mImageDisplay.bitmapData = BitmapManager.request(path);
				BitmapManager.lock(path, this);
				updateImageDisplay();
			}
			else
			{
				BitmapManager.startListeningForCompletion(path, fileListener);
				BitmapManager.loadAndLock(path, this);
			}
		}
	}
	private function loadImageListener(e:MouseEvent):Void
	{
		var file:File = File.applicationStorageDirectory.resolvePath(BitmapManager.rootDirectory); 
		file.addEventListener(Event.SELECT, completeLoadImageListener);
		file.browseForOpen("Load Image File", [new FileFilter("Image File", "*.png;*.jpg;*.jpeg;*.gif")]);
	}
	private function completeLoadImageListener(e:Event):Void
	{
		var root:File = new File().resolvePath(BitmapManager.rootDirectory);
		root.canonicalize();
		setImage(root.getRelativePath(cast e.target, true));
	}
	private function fileListener(e:BitmapManagerEvent):Void
	{
		mImageDisplay.bitmapData = BitmapManager.request(e.type);
		updateImageDisplay();
	}
	private function updateImageDisplay():Void
	{
		var r1:Float = mImageDisplay.bitmapData.width / mImageDisplay.bitmapData.height;
		var r2:Float = mWidth / (mHeight * 0.9);
		if (r1 > r2)
		{
			mImageDisplay.width = mWidth;
			mImageDisplay.height = mWidth / r1;
		}
		else
		{
			mImageDisplay.width = (mHeight * 0.9) * r1;
			mImageDisplay.height = (mHeight * 0.9);
		}
		updateOverlay();
	}
	
	public function resize(newWidth:Float, newHeight:Float):Void
	{
		graphics.clear();
		graphics.beginFill(0x333333);
		graphics.drawRect(0, 0, newWidth, newHeight);
		
		mColumnsLabel.width = newWidth * 0.1;
		mColumnsLabel.x = 0;
		mColumnsLabel.height = newHeight * 0.05;
		mColumnsInput.resize(newWidth * 0.05, newHeight * 0.05);
		TextTools.scaleTextToFit(mColumnsLabel);
		drawArrows(mColumnsArrows, newWidth * 0.02, newHeight * 0.05);
		mColumnsArrows[0].x = mColumnsLabel.x + mColumnsLabel.width;
		mColumnsInput.x = mColumnsArrows[0].x + mColumnsArrows[0].width;
		mColumnsArrows[1].x = mColumnsInput.x + mColumnsInput.width;
		mColumnsLabel.y =  mColumnsInput.y = mColumnsArrows[0].y = mColumnsArrows[1].y = newHeight * 0.9;
		
		mRowsLabel.width = newWidth * 0.1;
		mRowsLabel.x = newWidth * 0.2;
		mRowsLabel.height = newHeight * 0.05;
		mRowsInput.resize(newWidth * 0.05, newHeight * 0.05);
		TextTools.scaleTextToFit(mRowsLabel);
		drawArrows(mColumnsArrows, newWidth * 0.02, newHeight * 0.05);
		mRowsArrows[0].x = mRowsLabel.x + mRowsLabel.width;
		mRowsInput.x = mRowsArrows[0].x + mRowsArrows[0].width;
		mRowsArrows[1].x = mRowsInput.x + mRowsInput.width;
		mRowsLabel.y =  mRowsInput.y = mRowsArrows[0].y = mRowsArrows[1].y = newHeight * 0.9;
		
		mDelayLabel.width = newWidth * 0.1;
		mDelayLabel.x = newWidth * 0.4;
		mDelayLabel.height = newHeight * 0.05;
		mDelayInput.resize(newWidth * 0.05, newHeight * 0.05);
		TextTools.scaleTextToFit(mDelayLabel);
		mDelayInput.x = mDelayLabel.x + mDelayLabel.width;
		mDelayLabel.y = mDelayInput.y = newHeight * 0.9;
		
		mConfirmButton.resize(newWidth * 0.4, newHeight * 0.1);
		mConfirmButton.x = newWidth * 0.6;
		mConfirmButton.y = newHeight * 0.9;
		
		mImagePathContainer.graphics.clear();
		mImagePathContainer.graphics.lineStyle(3, 0xFFFFFF);
		mImagePathContainer.graphics.drawRect(1.5, 1.5, newWidth * 0.6 - 3, newHeight * 0.05 - 3);
		mImagePathContainer.x = 0;
		mImagePathContainer.y = newHeight * 0.95;
		mImagePathDisplay.x = 3;
		mImagePathDisplay.y = 3;
		mImagePathDisplay.width = newWidth * 0.6 - 6;
		mImagePathDisplay.height = newHeight * 0.05 - 6;
		TextTools.scaleTextToFit(mImagePathDisplay);
		
		mCloseButton.graphics.clear();
		mCloseButton.graphics.lineStyle(5, 0xFF0000);
		mCloseButton.graphics.beginFill(0x330000);
		mCloseButton.graphics.drawRect(2.5, 2.5, newWidth * 0.05 - 5, newHeight * 0.05 - 5);
		mCloseButton.graphics.moveTo(newWidth * 0.01, newHeight * 0.01);	mCloseButton.graphics.lineTo(newWidth * 0.04, newHeight * 0.04);
		mCloseButton.graphics.moveTo(newWidth * 0.04, newHeight * 0.01);	mCloseButton.graphics.lineTo(newWidth * 0.01, newHeight * 0.04);
		mCloseButton.x = newWidth * 0.95;
		
		if (mImageDisplay.bitmapData != null)
			updateImageDisplay();
	}
	
	private function closeButtonListener(e:MouseEvent):Void
	{
		dispatchEvent(new Event(Event.CLOSE));
	}
	
	private function confirmButtonListener(e:MouseEvent):Void
	{
		dispatchEvent(new Event(Event.COMPLETE));
	}
	
	private function drawArrows(arrows:Array<Sprite>, width:Float, height:Float):Void
	{
		for (s in arrows)
		{
			s.graphics.lineStyle(3, 0xFFFFFF);
			s.graphics.beginFill(0x333333);
			s.graphics.drawRect(0, 0, width, height);
		}
		arrows[0].graphics.beginFill(0x777777);
		arrows[0].graphics.moveTo(width * 0.7, height * 0.2);
		arrows[0].graphics.lineTo(width * 0.2, height * 0.5);
		arrows[0].graphics.lineTo(width * 0.7, height * 0.8);
		arrows[0].graphics.lineTo(width * 0.7, height * 0.2);
		arrows[1].graphics.beginFill(0x777777);
		arrows[1].graphics.moveTo(width * 0.3, height * 0.2);
		arrows[1].graphics.lineTo(width * 0.8, height * 0.5);
		arrows[1].graphics.lineTo(width * 0.3, height * 0.8);
		arrows[1].graphics.lineTo(width * 0.3, height * 0.2);
	}
	private function arrowListener(e:MouseEvent):Void
	{
		if (e.target == mColumnsArrows[0])
			mColumnsInput.value = Math.max(1, mColumnsInput.value - 1);
		else if (e.target == mColumnsArrows[1])
			++mColumnsInput.value;
		else if (e.target == mRowsArrows[0])		
			mRowsInput.value = Math.max(1, mRowsInput.value - 1);
		else
			++mRowsInput.value;
		updateOverlay();
	}
	
	private function valueChangeListener(e:Event):Void
	{
		mColumnsInput.value = Math.max(1, mColumnsInput.value);
		mRowsInput.value = Math.max(1, mRowsInput.value);
		updateOverlay();
	}
	
	private function updateOverlay():Void
	{
		var rows:Int = Math.floor(mRowsInput.value);
		var columns:Int = Math.floor(mColumnsInput.value);
		var w:Float = mImageDisplay.width / columns;
		var h:Float = mImageDisplay.height / rows;
		
		while (mFrameNumbers.length > (columns * rows))
		{
			var t:TextField = mFrameNumbers.pop();
			removeChild(t);
		}
		while (mFrameNumbers.length < (columns * rows))
		{
			var t:TextField = new TextField();
			t.text = Std.string(mFrameNumbers.length);
			addChild(t);
			mFrameNumbers.push(t);
			t.textColor = 0xFFFF00;
			t.alpha = 0.2;
			t.background = true;
			t.backgroundColor = 0x000000;
			t.border = true;
			t.borderColor = 0xFFFF00;
			t.mouseEnabled = false;
			t.defaultTextFormat.align = TextFormatAlign.CENTER;
		}
		
		for (i in 0...mFrameNumbers.length)
		{
			var t:TextField = mFrameNumbers[i];
			t.width = w;
			t.height = h;
			TextTools.scaleTextToFit(t);
			
			if (mRowsFirst)
			{
				t.x = Math.floor(i / rows) * w;
				t.y = ((i % rows) * h) ;
			}
			else
			{
				t.x = ((i % columns) * w);
				t.y = Math.floor(i / columns) * h;
			}
		}
		
		mImageDisplayOverlay.graphics.clear();
		mImageDisplayOverlay.graphics.lineStyle(1, 0xFF00FF, 0.8);
				
		for (x in 1...Math.floor(columns))
		{
			mImageDisplayOverlay.graphics.moveTo(w * x, 0);
			mImageDisplayOverlay.graphics.lineTo(w * x, mImageDisplay.height);
		}
		for (y in 1...Math.floor(rows))
		{
			mImageDisplayOverlay.graphics.moveTo(0, h * y);
			mImageDisplayOverlay.graphics.lineTo(mImageDisplay.width, h * y);
		}
		removeChild(mImageDisplayOverlay);
		addChild(mImageDisplayOverlay);
		removeChild(mCloseButton);
		addChild(mCloseButton);
	}
}