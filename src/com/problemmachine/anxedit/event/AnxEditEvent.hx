package com.problemmachine.anxedit.event;

import flash.events.Event;

class AnxEditEvent extends Event
{
	static public inline var IMPORT_IMAGE:String = "ImportImageEvent";
	static public inline var SAVE_UNDO_STATE:String = "SaveUndoEvent";
	static public inline var SET_FRAMERATE:String = "SetFramerate";
	
	public var data:Dynamic;
	
	public function new(type:String, ?data:Dynamic) 
	{
		this.data = data;
		super(type);
	}
	
}